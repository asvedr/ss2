# Serialization cases
there are two cases of serialzation:
- as a param
- as a return value

# Std types serialization
standart types can be used as a return value and as param also. As a param standart type will be translated in single atom(not a tuple).
[List of std types](/docs/types.md)

# Struct serialzation
## Serialize struct on select
```go
type T struct {
    Key string
    Val int
}
t, err := ss2.FetchOne[T](
    db,
    "select k, v from tbl where id = ?",
    123,
)
```

`ss2` engine detects that `T` contains 2 fields (string, int) and this fields will be scanned from `*sql.Rows` **BY POSITION**. So in select they can have different names but `integer` value must be after `string` value.

## Serialize struct on insert
```go
type T struct {
    Key int
    Val int
}
err := ss2.Exec(
    db,
    "insert into tbl (key, val) values (?, ?)",
    T{Key: 1, Val: 2},
)
```

`ss2` engine will process the `T` instance as a tuple of `(int, int)`.
