package ss2_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

type NewTblFkDest struct {
	Id   int `pk:"t"`
	Str  string
	Meta struct {
		Name ss2.None `value:"tbl_dst"`
	}
}

type NewTblWithoutFk struct {
	Id int `pk:"t"`
	// Fk   int `fk:"tbl_dst.Id"`
	Meta struct {
		Name ss2.None `value:"tbl_with_fk"`
	}
}

type NewTblWithFk struct {
	Id   int  `pk:"t"`
	Fk   *int `fk:"tbl_dst.Id"`
	Meta struct {
		Name ss2.None `value:"tbl_with_fk"`
	}
}

type TblInvalidFkTbl struct {
	Id int `pk:"t"`
	Fk int `fk:"xxx.Id"`
}

type TblInvalidFkColumn struct {
	Id int `pk:"t"`
	Fk int `fk:"tbl_dst.xxx"`
}

type TblInvalidFkType struct {
	Id int    `pk:"t"`
	Fk string `fk:"tbl_dst.Id"`
}

func TestCreateWithForeignKey(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)
	err = ss2.MigrationManager{Db: db}.CreateTables(
		NewTblFkDest{}, NewTblWithFk{},
	)
	assert.NoError(t, err)
	err = ss2.Exec(
		db,
		`INSERT INTO tbl_with_fk (fk) VALUES (1)`,
	)
	assert.ErrorIs(t, err, ss2.ErrForeignKey)

	err = ss2.Exec(
		db,
		"INSERT INTO tbl_dst (id, str) VALUES (1, 'a')",
	)
	assert.NoError(t, err)
	err = ss2.Exec(
		db,
		`INSERT INTO tbl_with_fk (fk) VALUES (1)`,
	)
	assert.NoError(t, err)
}

func TestAddFkColumn(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)
	mm := ss2.MigrationManager{Db: db}
	m1, err := mm.GenerateMigration(
		"m1",
		nil,
		NewTblFkDest{},
		NewTblWithoutFk{},
	)
	// println(m1)
	assert.NoError(t, err)
	m2, err := mm.GenerateMigration(
		"m2",
		[]string{m1},
		NewTblFkDest{},
		NewTblWithFk{},
	)
	assert.NoError(t, err)

	assert.NoError(t, mm.ApplyMigrations([]string{m1, m2}))

	err = ss2.Exec(
		db,
		`INSERT INTO tbl_with_fk (fk) VALUES (1)`,
	)
	assert.ErrorIs(t, err, ss2.ErrForeignKey)
}

var tbl_nf = "Inconsistent migration state: fk xxx.id table not found"
var col_nf = "Inconsistent migration state: fk tbl_dst.xxx column not found"
var tp_err = "Inconsistent migration state: fk tbl_dst.id type mismatch"

var err_cases = []struct {
	Table any
	Err   string
}{
	{Table: TblInvalidFkTbl{}, Err: tbl_nf},
	{Table: TblInvalidFkColumn{}, Err: col_nf},
	{Table: TblInvalidFkType{}, Err: tp_err},
}

func TestFkErrors(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)
	mm := ss2.MigrationManager{Db: db}

	body := func(t *testing.T, i int) {
		tbls := []any{NewTblFkDest{}, err_cases[i].Table}
		_, err = mm.GenerateMigration("xxx", nil, tbls...)
		assert.Equal(t, err_cases[i].Err, err.Error())
	}

	for i := range err_cases {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			body(t, i)
		})
	}
}
