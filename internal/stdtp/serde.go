package stdtp

import (
	"reflect"
	"time"

	"gitlab.com/asvedr/ss2/internal/atomic_wrapper"
	"gitlab.com/asvedr/ss2/internal/proto"
)

// int, uint, float, bool, string
type as_is[T any] struct{}

type as_is_opt[T any] struct{}

type time_serde struct{}

type time_serde_opt struct{}

type nil_serde struct{}

var NilSerde = atomic_wrapper.W[nil_serde]{}

var Map = map[reflect.Type]proto.IAtomicSerDe{
	tp_name[int]():        atomic_wrapper.W[as_is[int]]{},
	tp_name[*int]():       atomic_wrapper.W[as_is_opt[int]]{},
	tp_name[int8]():       atomic_wrapper.W[as_is[int8]]{},
	tp_name[*int8]():      atomic_wrapper.W[as_is_opt[int8]]{},
	tp_name[int16]():      atomic_wrapper.W[as_is[int16]]{},
	tp_name[*int16]():     atomic_wrapper.W[as_is_opt[int16]]{},
	tp_name[int32]():      atomic_wrapper.W[as_is[int32]]{},
	tp_name[*int32]():     atomic_wrapper.W[as_is_opt[int32]]{},
	tp_name[int64]():      atomic_wrapper.W[as_is[int64]]{},
	tp_name[*int64]():     atomic_wrapper.W[as_is_opt[int64]]{},
	tp_name[uint]():       atomic_wrapper.W[as_is[uint]]{},
	tp_name[*uint]():      atomic_wrapper.W[as_is_opt[uint]]{},
	tp_name[uint8]():      atomic_wrapper.W[as_is[uint8]]{},
	tp_name[*uint8]():     atomic_wrapper.W[as_is_opt[uint8]]{},
	tp_name[uint16]():     atomic_wrapper.W[as_is[uint16]]{},
	tp_name[*uint16]():    atomic_wrapper.W[as_is_opt[uint16]]{},
	tp_name[uint32]():     atomic_wrapper.W[as_is[uint32]]{},
	tp_name[*uint32]():    atomic_wrapper.W[as_is_opt[uint32]]{},
	tp_name[uint64]():     atomic_wrapper.W[as_is[uint64]]{},
	tp_name[*uint64]():    atomic_wrapper.W[as_is_opt[uint64]]{},
	tp_name[float32]():    atomic_wrapper.W[as_is[float32]]{},
	tp_name[*float32]():   atomic_wrapper.W[as_is_opt[float32]]{},
	tp_name[float64]():    atomic_wrapper.W[as_is[float64]]{},
	tp_name[*float64]():   atomic_wrapper.W[as_is_opt[float64]]{},
	tp_name[bool]():       atomic_wrapper.W[as_is[bool]]{},
	tp_name[*bool]():      atomic_wrapper.W[as_is_opt[bool]]{},
	tp_name[string]():     atomic_wrapper.W[as_is[string]]{},
	tp_name[*string]():    atomic_wrapper.W[as_is_opt[string]]{},
	tp_name[time.Time]():  atomic_wrapper.W[time_serde]{},
	tp_name[*time.Time](): atomic_wrapper.W[time_serde_opt]{},
}

func (as_is[T]) ToParam(t any) any {
	return t
}

func (as_is[T]) Receiver() any {
	var t T
	return &t
}

func (as_is[T]) FromParam(ptr any) any {
	return *ptr.(*T)
}

func (as_is_opt[T]) ToParam(t any) any {
	if t == nil {
		return nil
	}
	ptr := t.(*T)
	if ptr == nil {
		return nil
	}
	return *ptr
}

func (as_is_opt[T]) Receiver() any {
	var t *T
	return &t
}

func (as_is_opt[T]) FromParam(ptr any) any {
	return *ptr.(**T)
}

func (self time_serde) ToParam(t any) any {
	return t.(time.Time).UTC().UnixMilli()
}

func (self time_serde) Receiver() any {
	var t int64
	return &t
}

func (self time_serde) FromParam(ptr any) any {
	return time.UnixMilli(*ptr.(*int64)).UTC()
}

func (self time_serde_opt) ToParam(t any) any {
	if t == nil {
		return nil
	}
	ptr := t.(*time.Time)
	if ptr == nil {
		return nil
	}
	return ptr.UTC().UnixMilli()
}

func (self time_serde_opt) Receiver() any {
	var t *int64
	return &t
}

func (self time_serde_opt) FromParam(ptr any) any {
	i_ptr := *ptr.(**int64)
	if i_ptr == nil {
		return nil
	}
	tm := time.UnixMilli(*i_ptr).UTC()
	return &tm
}

func (nil_serde) ToParam(t any) any {
	return t
}

func (nil_serde) Receiver() any {
	panic("Receiver is not implemnted for nil")
}

func (nil_serde) FromParam(ptr any) any {
	panic("FromParam is not implemnted for nil")
}
