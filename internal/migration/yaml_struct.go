package migration

import (
	"errors"
	"fmt"
	"sort"

	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/table"
)

type YamlMigration struct {
	Name       string          `yaml:"name"`
	Fake       *bool           `yaml:"fake,omitempty"`
	Operations []YamlOperation `yaml:"operations"`
}

type YamlOperation struct {
	Type       string       `yaml:"type"`
	Table      *string      `yaml:"table,omitempty"`
	NewColumn  *YamlColumn  `yaml:"new_column,omitempty"`
	Name       *string      `yaml:"name,omitempty"`
	Columns    []string     `yaml:"columns,omitempty,flow"`
	Unique     *bool        `yaml:"unique,omitempty"`
	NewColumns []YamlColumn `yaml:"new_columns,omitempty"`
	Column     *string      `yaml:"column,omitempty"`
	Index      *string      `yaml:"index,omitempty"`
	Sql        *string      `yaml:"sql,omitempty"`
}

const (
	TypeAddColumn      = "add_column"
	TypeAddIndex       = "add_index"
	TypeAddTable       = "add_table"
	TypeDropColumn     = "drop_column"
	TypeDropIndex      = "drop_index"
	TypeDropTable      = "drop_table"
	TypeSql            = "sql"
	TypeRedefineColumn = "redefine_column"
)

type YamlColumn struct {
	Name       string  `yaml:"name"`
	Type       string  `yaml:"type"`
	PrimaryKey *bool   `yaml:"pk,omitempty"`
	NotNull    *bool   `yaml:"not_null,omitempty"`
	Default    *string `yaml:"db_default,omitempty"`
	ForeignKey *YamlFK `yaml:"fk,omitempty"`
}

type YamlFK struct {
	Table  string `yaml:"table"`
	Column string `yaml:"column"`
}

func (self YamlOperation) Compile() (proto.IMigrationOperation, error) {
	switch self.Type {
	case TypeAddColumn:
		return self.CompileAddColumn()
	case TypeAddIndex:
		return self.CompileAddIndex()
	case TypeAddTable:
		return self.CompileAddTable()
	case TypeDropColumn:
		return self.CompileDropColumn()
	case TypeDropIndex:
		return self.CompileDropIndex()
	case TypeDropTable:
		return self.CompileDropTable()
	case TypeSql:
		return self.CompileSql()
	case TypeRedefineColumn:
		return self.CompileRedefineColumn()
	}
	return nil, errors.New("invalid operation type")
}

func (self YamlOperation) CompileAddColumn() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	if self.NewColumn == nil {
		return nil, errors.New("missing new column")
	}
	mig := AddColumn{
		Table:  *self.Table,
		Column: self.NewColumn.Compile(),
	}
	return mig, nil
}

func (self YamlOperation) CompileRedefineColumn() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	if self.NewColumn == nil {
		return nil, errors.New("missing new column")
	}
	mig := RedefineColumn{
		Table:  *self.Table,
		Column: self.NewColumn.Compile(),
	}
	return mig, nil
}

func (self YamlOperation) CompileAddIndex() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	if len(self.Columns) == 0 {
		return nil, errors.New("missing columns")
	}
	if self.Name == nil {
		return nil, errors.New("missing index name")
	}
	mig := AddIndex{
		Table: *self.Table,
		Index: table.Index{
			Name:    *self.Name,
			Columns: self.Columns,
			Unique:  opt2bool(self.Unique),
		},
	}
	return mig, nil
}

func (self YamlOperation) CompileAddTable() (proto.IMigrationOperation, error) {
	if self.Name == nil {
		return nil, errors.New("missing table name")
	}
	columns := make(map[string]table.Column, len(self.Columns))
	for _, column := range self.NewColumns {
		compiled := column.Compile()
		columns[compiled.Name] = compiled
	}
	mig := AddTable{
		Table: table.Table{
			Name:    *self.Name,
			Columns: columns,
		},
	}
	return mig, nil
}

func (self YamlOperation) CompileDropColumn() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	if self.Column == nil {
		return nil, errors.New("missing column name")
	}
	mig := DropColumn{
		Table:  *self.Table,
		Column: *self.Column,
	}
	return mig, nil
}

func (self YamlOperation) CompileDropIndex() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	if self.Index == nil {
		return nil, errors.New("missing index name")
	}
	mig := DropIndex{
		Table: *self.Table,
		Index: *self.Index,
	}
	return mig, nil
}

func (self YamlOperation) CompileDropTable() (proto.IMigrationOperation, error) {
	if self.Table == nil {
		return nil, errors.New("missing table name")
	}
	mig := DropTable{
		Table: *self.Table,
	}
	return mig, nil
}

func (self YamlOperation) CompileSql() (proto.IMigrationOperation, error) {
	if self.Sql == nil {
		return nil, errors.New("missing sql")
	}
	return Sql{Sql: *self.Sql}, nil
}

func (self YamlColumn) Compile() table.Column {
	var foreign_key *table.ForeignKey
	if self.ForeignKey != nil {
		foreign_key = &table.ForeignKey{
			Table:  self.ForeignKey.Table,
			Column: self.ForeignKey.Column,
		}
	}
	return table.Column{
		Name:       self.Name,
		Type:       self.Type,
		PrimaryKey: opt2bool(self.PrimaryKey),
		NotNull:    opt2bool(self.NotNull),
		Default:    self.Default,
		ForeignKey: foreign_key,
	}
}

func column2yaml(column table.Column) YamlColumn {
	var fk *YamlFK
	if column.ForeignKey != nil {
		fk = &YamlFK{
			Table:  column.ForeignKey.Table,
			Column: column.ForeignKey.Column,
		}
	}
	return YamlColumn{
		Name:       column.Name,
		Type:       column.Type,
		PrimaryKey: bool2opt(column.PrimaryKey),
		NotNull:    bool2opt(column.NotNull),
		Default:    column.Default,
		ForeignKey: fk,
	}
}

func columns2yaml(columns map[string]table.Column) []YamlColumn {
	yml := make([]YamlColumn, len(columns))
	var pk string
	var order []string
	for key, col := range columns {
		if col.PrimaryKey {
			pk = key
		} else {
			order = append(order, key)
		}
	}
	sort.StringSlice(order).Sort()
	if pk != "" {
		order = append([]string{pk}, order...)
	}
	for i, name := range order {
		yml[i] = column2yaml(columns[name])
	}
	return yml
}

func ptr[T any](t T) *T {
	return &t
}

func operation2yaml(op_int proto.IMigrationOperation) (YamlOperation, error) {
	switch op := op_int.(type) {
	case AddColumn:
		return YamlOperation{
			Type:      TypeAddColumn,
			Table:     &op.Table,
			NewColumn: ptr(column2yaml(op.Column)),
		}, nil
	case AddIndex:
		return YamlOperation{
			Type:    TypeAddIndex,
			Table:   &op.Table,
			Columns: op.Index.Columns,
			Unique:  bool2opt(op.Index.Unique),
			Name:    &op.Index.Name,
		}, nil
	case AddTable:
		return YamlOperation{
			Type:       TypeAddTable,
			Name:       &op.Table.Name,
			NewColumns: columns2yaml(op.Table.Columns),
		}, nil
	case DropColumn:
		return YamlOperation{
			Type:   TypeDropColumn,
			Table:  &op.Table,
			Column: &op.Column,
		}, nil
	case DropIndex:
		return YamlOperation{
			Type:  TypeDropIndex,
			Table: &op.Table,
			Index: &op.Index,
		}, nil
	case DropTable:
		return YamlOperation{
			Type:  TypeDropTable,
			Table: &op.Table,
		}, nil
	// SQL migration can not be generated
	// case Sql:
	// 	return YamlOperation{
	// 		Type: TypeSql,
	// 		Sql:  &op.Sql,
	// 	}, nil
	case RedefineColumn:
		return YamlOperation{
			Type:      TypeRedefineColumn,
			Table:     &op.Table,
			NewColumn: ptr(column2yaml(op.Column)),
		}, nil
	}
	err := fmt.Errorf("unknown operation type: %T", op_int)
	return YamlOperation{}, err
}
