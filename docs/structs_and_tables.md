# Struct declaration
```go
type T struct {
    Key string
    Val int
}
t, err := ss2.FetchOne[T](
    db,
    "select k, v from tbl where id = ?",
    123,
)
```
`T` can be used for select and insert. In both cases `T` can be interpreted as tuple `(string, int)`

Struct fields can be only from [standart type list](/docs/types.md)

Table declaration can also be used for select/insert struct

# Table declaration
```go
type TableSchema struct {
    // primary key
    Id      int     `pk:"t"`
    // indexed field
    F1      string  `index:"t"`
    // field with unique index
    F2      string  `unique:"t"`
    // optional field
    F3      *int
    // field with default value
    F4      float64 `db_default:"1.5"`
    // foreign key field
    F5      int     `fk:"other_table.column"`
    // Table meta information [Optional]
    Meta    struct {
        // Custom table name
        Name       ss2.None `value:"tbl"`
        // Custom index declaration (unique together F1+F2)
        Index_i1   ss2.None `value:"F1, F2" unique:"t"`
	}
}
```

[This struct can be used in migration manager directly for db initialization or for migration generation](/docs/migrations.md).