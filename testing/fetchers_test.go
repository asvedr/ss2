package ss2_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

type KeyVal struct {
	Key string
	Val string
}

func f_ExecReturnId(db ss2.ICursor, t *testing.T) {
	id, err := ss2.ExecReturnId(db, "insert into tbl (key, val) VALUES (?, ?)", "a", "'")
	assert.Nil(t, err)
	row, err := ss2.FetchOne[KeyVal](
		db,
		"SELECT key, val FROM tbl WHERE id = ?",
		id,
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		KeyVal{Key: "a", Val: "'"},
		row,
	)
	_, err = ss2.FetchOne[KeyVal](
		db,
		"SELECT key, val FROM tbl WHERE id = ?",
		id+100,
	)
	assert.ErrorIs(t, err, ss2.ErrNotFound)
	opt_row, err := ss2.FetchOpt[KeyVal](
		db,
		"SELECT key, val FROM tbl WHERE id = ?",
		id+100,
	)
	assert.Nil(t, err)
	assert.Nil(t, opt_row)
}

func TestExecReturnId(t *testing.T) {
	db := setup()
	f_ExecReturnId(db, t)
	f := func(l ss2.ICursor) error {
		f_ExecReturnId(l, t)
		return nil
	}
	db.WithLocked(f)
}

func TestExecReturnIdErr(t *testing.T) {
	db := setup()
	_, err := ss2.ExecReturnId(db, "SELECT abc FROM def")
	assert.Error(t, err)
}

func f_FetchSlice(db ss2.ICursor, t *testing.T) {
	ss2.Exec(
		db,
		"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
		1, "a",
		2, "b",
	)
	vals, err := ss2.FetchSlice[KeyVal](
		db,
		"SELECT key, val FROM tbl ORDER BY key",
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		[]KeyVal{{Key: "1", Val: "a"}, {Key: "2", Val: "b"}},
		vals,
	)
}

func TestFetchSlice(t *testing.T) {
	f_FetchSlice(setup(), t)
	f := func(l ss2.ICursor) error {
		f_FetchSlice(l, t)
		return nil
	}
	setup().WithLocked(f)
}

func f_FetchMap(db ss2.ICursor, t *testing.T) {
	err := ss2.Exec(
		db,
		"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
		1, "a",
		2, "b",
	)
	if err != nil {
		t.Fatal(err)
	}
	vals, err := ss2.FetchMap[string, string](
		db,
		"SELECT key, val FROM tbl ORDER BY key",
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		map[string]string{"1": "a", "2": "b"},
		vals,
	)
}

func TestFetchMap(t *testing.T) {
	f_FetchMap(setup(), t)
	f := func(l ss2.ICursor) error {
		f_FetchMap(l, t)
		return nil
	}
	setup().WithLocked(f)
}

func TestTxCommit(t *testing.T) {
	db := setup()

	db.WithTxLocked(func(l ss2.ICursor) error {
		f_FetchMap(l, t)
		return nil
	})

	vals, err := ss2.FetchMap[string, string](
		db,
		"SELECT key, val FROM tbl ORDER BY key",
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		map[string]string{"1": "a", "2": "b"},
		vals,
	)
}

func TestTxRollback(t *testing.T) {
	db := setup()

	db.WithTxLocked(func(l ss2.ICursor) error {
		f_FetchMap(l, t)
		return errors.New("some error")
	})

	vals, err := ss2.FetchMap[string, string](
		db,
		"SELECT key, val FROM tbl ORDER BY key",
	)
	assert.Nil(t, err)
	assert.Equal(t, 0, len(vals))
}

func TestIter(t *testing.T) {
	db := setup()
	ss2.Exec(
		db,
		"INSERT INTO tbl (key, val) VALUES (?, ?), (?, ?)",
		1, "a",
		2, "b",
	)
	res := ""
	err := ss2.Iter(
		db,
		func(row KeyVal) error {
			res += row.Key + " " + row.Val + " "
			return nil
		},
		"select key, val from tbl order by key",
	)
	assert.Nil(t, err)
	assert.Equal(t, "1 a 2 b ", res)
}

func TestExecParams(t *testing.T) {
	db := setup()
	obj := KeyVal{Key: "a", Val: "b"}
	err := ss2.Exec(
		db,
		"INSERT INTO tbl (key, val) VALUES (?, ?)",
		obj,
	)
	assert.NoError(t, err)
	row, err := ss2.FetchOne[KeyVal](db, "SELECT key, val FROM tbl")
	assert.NoError(t, err)
	assert.Equal(t, obj, row)
}
