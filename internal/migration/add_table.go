package migration

import (
	"maps"

	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type AddTable struct {
	Table table.Table
}

func (self AddTable) ChangeState(state *state.State) error {
	state.Tables[self.Table.Name] = table.Table{
		Name:    self.Table.Name,
		Columns: maps.Clone(self.Table.Columns),
	}
	state.CreateOrder = append(state.CreateOrder, self.Table.Name)
	return nil
}

func (self AddTable) Query() string {
	return self.Table.CreateQuery()
}
