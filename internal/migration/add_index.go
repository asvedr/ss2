package migration

import (
	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type AddIndex struct {
	Table string
	Index table.Index
}

func (self AddIndex) ChangeState(state *state.State) error {
	tbl := state.Tables[self.Table]
	index := self.Index
	index.Columns = append([]string{}, index.Columns...)
	tbl.Indexes = append(tbl.Indexes, index)
	state.Tables[self.Table] = tbl
	return nil
}

func (self AddIndex) Query() string {
	return table.CreateIndex(self.Table, self.Index)
}
