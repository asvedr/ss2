import sys
from typing import Iterable, Tuple


FILE = "coverage.html"
TARGET_COVERAGE = 93.0

EXCLUDE_FILES = [
    "gitlab.com/asvedr/ss2/mocks",
    "gitlab.com/asvedr/ss2/lib.go",
]


def extract_num(src: Iterable[str]) -> Iterable[Tuple[str, float]]:
    for line in src:
        if any(f in line for f in EXCLUDE_FILES):
            continue
        if '<option value="file' in line:
            name = line.split(">")[1].split(" ")[0]
            num = line.split(" (")[1].split("%)")[0]
            yield (name, float(num))


def main():
    with open(FILE) as handler:
        cov_list = dict(extract_num(handler))
    min_coverage = min(cov_list.values())
    avg_coverage = sum(cov_list.values()) / len(cov_list)
    print(f"avg coverage: {avg_coverage}")

    if min_coverage < TARGET_COVERAGE:
        for name, cov in cov_list.items():
            if cov < TARGET_COVERAGE:
                print(f"FAILED ON {name}: {cov} < {TARGET_COVERAGE}")
        sys.exit(1)
    print(f"OK min coverage: {min_coverage} >= {TARGET_COVERAGE}")

main()
