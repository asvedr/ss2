# Safe sqlite 2

- [How to query](/docs/queries.md)
- [How to use migrations](/docs/migrations.md)
- [Transactions and locks](/docs/transactions.md)
- [Std types](/docs/types.md)
- [Structs and tables](/docs/structs_and_tables.md)
- [Serialization](/docs/serialization.md)
