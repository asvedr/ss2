package errs_test

import (
	"testing"

	"gitlab.com/asvedr/ss2/internal/errs"
)

var objs = []error{
	errs.ErrNotFound{},
	errs.ErrUniqConstraint{},
	errs.ErrInconsistentMigration{},
	errs.ErrInconsistentMigrationState{},
	errs.ErrColumnChanged{},
	errs.ErrMigrationNameDuplicated{},
	errs.ErrForeignKey{},
}

func TestCast(t *testing.T) {
	for _, obj := range objs {
		_ = obj.Error()
	}
}
