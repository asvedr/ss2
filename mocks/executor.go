package mocks

import (
	"database/sql"

	"github.com/stretchr/testify/mock"
)

type Executor struct {
	mock.Mock
}

func (self *Executor) Exec(query string, args ...any) (sql.Result, error) {
	called := self.Called(query, args)
	var res sql.Result
	any_res := called.Get(0)
	if any_res != nil {
		res = any_res.(sql.Result)
	}
	return res, called.Error(1)
}

func (self *Executor) Query(query string, args ...any) (*sql.Rows, error) {
	called := self.Called(query, args)
	var res *sql.Rows
	any_res := called.Get(0)
	if any_res != nil {
		res = any_res.(*sql.Rows)
	}
	return res, called.Error(1)
}
