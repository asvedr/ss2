package state

import (
	"fmt"
	"slices"

	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/table"
)

type State struct {
	Tables      map[string]table.Table
	CreateOrder []string
}

func New() *State {
	return &State{
		Tables: map[string]table.Table{},
	}
}

func (self *State) RevOrder() []string {
	clone := slices.Clone(self.CreateOrder)
	slices.Reverse(clone)
	return clone
}

func (self *State) Validate() error {
	co_len := len(self.CreateOrder)
	for i := range co_len - 1 {
		for j := i + 1; j < co_len; j++ {
			if self.CreateOrder[i] == self.CreateOrder[j] {
				msg := "duplicate table name: " + self.CreateOrder[i]
				return errs.ErrInconsistentMigrationState{Reason: msg}
			}
		}
	}
	for _, tbl := range self.Tables {
		err := self.validate_columns(tbl)
		if err != nil {
			return err
		}
		err = validate_indexes(tbl)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self *State) validate_columns(tbl table.Table) error {
	var prim_keys []string
	ret_err := func(tmpl string, args ...any) error {
		msg := fmt.Sprintf(tmpl, args...)
		return errs.ErrInconsistentMigrationState{Reason: msg}
	}
	for _, colums := range tbl.Columns {
		if colums.PrimaryKey {
			prim_keys = append(prim_keys, colums.Name)
		}
		if colums.ForeignKey != nil {
			fk := *colums.ForeignKey
			fk_tbl, ok := self.Tables[fk.Table]
			if !ok {
				return ret_err("fk %s table not found", fk)
			}
			fk_col, ok := fk_tbl.Columns[fk.Column]
			if !ok {
				return ret_err("fk %s column not found", fk)
			}
			if fk_col.Type != colums.Type {
				return ret_err("fk %s type mismatch", fk)
			}
		}
	}
	if len(prim_keys) == 0 {
		return ret_err("table %s has no primary key", tbl.Name)
	}
	if len(prim_keys) > 1 {
		return ret_err("table %s has many primary keys", tbl.Name)
	}
	return nil
}

func validate_indexes(tbl table.Table) error {
	for i := range len(tbl.Indexes) - 1 {
		for j := i + 1; j < len(tbl.Indexes); j++ {
			if tbl.Indexes[i].Name == tbl.Indexes[j].Name {
				msg := fmt.Sprintf(
					"table %s duplicate index name: %s",
					tbl.Name,
					tbl.Indexes[i].Name,
				)
				return errs.ErrInconsistentMigrationState{Reason: msg}
			}
		}
	}
	for _, index := range tbl.Indexes {
		err := validate_index_columns(tbl, index)
		if err != nil {
			return err
		}
	}
	return nil
}

func validate_index_columns(
	tbl table.Table,
	index table.Index,
) error {
	if len(index.Columns) == 0 {
		msg := fmt.Sprintf(
			"table %s, index %s has no columns",
			tbl.Name,
			index.Name,
		)
		return errs.ErrInconsistentMigrationState{
			Reason: msg,
		}
	}
	for _, column := range index.Columns {
		found := false
		for _, c := range tbl.Columns {
			if c.Name == column {
				found = true
				break
			}
		}
		if !found {
			msg := fmt.Sprintf(
				"table %s, index %s column not found in table: %s",
				tbl.Name,
				index.Name,
				column,
			)
			return errs.ErrInconsistentMigrationState{Reason: msg}
		}
	}
	return nil
}
