package migration

import (
	"fmt"

	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type DropIndex struct {
	Table string
	Index string
}

func (self DropIndex) ChangeState(state *state.State) error {
	tbl := state.Tables[self.Table]
	i := self.get_index(tbl, self.Index)
	if i < 0 {
		return fmt.Errorf(
			"can not find index %s:%s",
			self.Table,
			self.Index,
		)
	}
	indexes := tbl.Indexes
	tbl.Indexes = append(indexes[:i], indexes[i+1:]...)
	state.Tables[self.Table] = tbl
	return nil
}

func (self DropIndex) Query() string {
	return `DROP INDEX IF EXISTS ` + self.Index
}

func (self DropIndex) get_index(tbl table.Table, name string) int {
	for i, idx := range tbl.Indexes {
		if idx.Name == name {
			return i
		}
	}
	return -1
}
