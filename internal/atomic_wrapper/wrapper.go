package atomic_wrapper

import (
	"gitlab.com/asvedr/ss2/internal/proto"
)

type W[T proto.IAtomic] struct {
	t T
}

func (self W[T]) ToParams(t any) []any {
	return []any{self.ToParam(t)}
}

func (self W[T]) Receivers() []any {
	return []any{self.Receiver()}
}

func (self W[T]) FromParams(params []any) any {
	return self.FromParam(params[0])
}

func (self W[T]) ToParam(t any) any {
	return self.t.ToParam(t)
}

func (self W[T]) Receiver() any {
	return self.t.Receiver()
}

func (self W[T]) FromParam(param any) any {
	return self.t.FromParam(param)
}
