package errs

import "fmt"

type ErrNotFound struct{}
type ErrUniqConstraint struct{}
type ErrForeignKey struct{}
type ErrInconsistentMigration struct {
	Name string
	Step int
}
type ErrInconsistentMigrationState struct {
	Reason string
}
type ErrColumnChanged struct {
	Table     string
	OldColumn string
	NewColumn string
}
type ErrMigrationNameDuplicated struct {
	Name string
}

func (self ErrColumnChanged) Error() string {
	return fmt.Sprintf(
		"Detected column change(table=%s):, %s -> %s",
		self.Table,
		self.OldColumn,
		self.NewColumn,
	)
}

func (ErrNotFound) Error() string {
	return "not found"
}

func (self ErrInconsistentMigration) Error() string {
	return fmt.Sprintf(
		`migration "%s":%d`,
		self.Name,
		self.Step,
	)
}

func (ErrForeignKey) Error() string {
	return "Foreign key constraint failed"
}

func (ErrUniqConstraint) Error() string {
	return "Unique constraint failed"
}

func (self ErrInconsistentMigrationState) Error() string {
	return "Inconsistent migration state: " + self.Reason
}

func (self ErrMigrationNameDuplicated) Error() string {
	return "Duplicated migration name: " + self.Name
}
