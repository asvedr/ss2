# General information
Sqlite db can be locked only for one request per time.
So all requests make the lock.

# Autolock
```go
db, _ := ss2.NewDb(":memory:")
ss2.Exec("insert ...")
```
In this case DB mutex will be locked due request.

# Explicit lock
```go
db, err := ss2.NewDb(":memory:")
f := func(crs func(ss2.ICursor) error) {
    crs.Exec("inser ...")
    crs.Exec("inser ...")
    ...
}
err = db.WithLocked(f)
```
In this case only one lock will be used due all `f` function

# Transaction lock
```go
db, err := ss2.NewDb(":memory:")
f := func(crs func(ss2.ICursor) error) {
    crs.Exec("inser ...")
    crs.Exec("inser ...")
    ...
}
err = db.WithTxLocked(f)
```
In this case only one lock will be used due all `f` function + function will execute in transaction
