package ss2_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/asvedr/ss2/internal/migration"
	"gitlab.com/asvedr/ss2/internal/migration_manager"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/mocks"
)

func TestMigManagerApplyEmpty(t *testing.T) {
	mm := migration_manager.New(nil, false)
	err := mm.ApplyMigrations([]proto.IMigration{})
	assert.NoError(t, err)
}

func TestMigManagerErrTest(t *testing.T) {
	crs := new(mocks.Cursor)
	exp := errors.New("err")
	crs.On("Exec", mock.Anything).Return(exp)
	mm := migration_manager.New(crs, false)
	err := mm.ApplyMigrations([]proto.IMigration{
		&migration.Migration{
			Name: "test",
			Operations: []proto.IMigrationOperation{
				migration.DropColumn{
					Table: "t", Column: "c",
				},
			},
		},
	})
	assert.Equal(t, exp, err)
}

func TestMigManagerFetchLastMig(t *testing.T) {
	crs := new(mocks.Cursor)
	exp := errors.New("err")
	crs.On("Exec", mock.Anything).Return(nil)
	crs.On(
		"Iter",
		`SELECT name, step FROM lastmigration LIMIT 1`,
	).Return(exp)
	mm := migration_manager.New(crs, false)
	err := mm.ApplyMigrations([]proto.IMigration{
		&migration.Migration{
			Name: "test",
			Operations: []proto.IMigrationOperation{
				migration.DropColumn{
					Table: "t", Column: "c",
				},
			},
		},
	})
	assert.Equal(t, exp, err)
}

func TestCreateTablesErr(t *testing.T) {
	crs := new(mocks.Cursor)
	exp := errors.New("err")
	crs.On("Exec", mock.Anything).Return(exp)
	mm := migration_manager.New(crs, false)
	err := mm.CreateTables([]any{Tbl1v1{}})
	assert.Equal(t, exp, err)
}

func TestInvalidOldState(t *testing.T) {
	crs := new(mocks.Cursor)
	mm := migration_manager.New(crs, false)
	_, err := mm.GenerateMigration(
		"xxx",
		[]proto.IMigration{
			&migration.Migration{
				Name: "test",
				Operations: []proto.IMigrationOperation{
					migration.DropColumn{
						Table: "t", Column: "c",
					},
				},
			},
		},
		[]any{Tbl1v1{}},
	)
	assert.Equal(t, "can not find column t:c", err.Error())
}
