package db

import (
	"database/sql"
	"sync"

	"gitlab.com/asvedr/ss2/internal/cursor"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/registry"
	_ "modernc.org/sqlite"
)

type Db struct {
	db       *sql.DB
	registry proto.IRegistry
	mtx      *sync.Mutex
}

func New(path string) (Db, error) {
	src, err := sql.Open("sqlite", path)
	if err != nil {
		return Db{}, err
	}
	db := Db{
		db:       src,
		registry: registry.New(),
		mtx:      &sync.Mutex{},
	}
	return db, nil
}

func (self Db) WithTxLocked(f func(proto.ICursor) error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	tx, err := self.db.Begin()
	if err != nil {
		return err
	}
	err = f(cursor.New(self.registry, tx))
	if err == nil {
		err = tx.Commit()
	}
	if err != nil {
		tx.Rollback()
	}
	return err
}

func (self Db) WithLocked(f func(proto.ICursor) error) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	return f(cursor.New(self.registry, self.db))
}

func (self Db) Exec(query string, args ...any) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	_, err := self.db.Exec(query, args...)
	return err
}

func (self Db) ExecReturnId(query string, args ...any) (int64, error) {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	res, err := self.db.Exec(query, args...)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (self Db) Iter(f func(*sql.Rows) error, query string, args ...any) error {
	self.mtx.Lock()
	defer self.mtx.Unlock()
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self Db) Registry() proto.IRegistry {
	return self.registry
}
