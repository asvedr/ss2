package proto

import (
	"database/sql"
	"reflect"

	"gitlab.com/asvedr/ss2/internal/state"
)

type IExecutor interface {
	Exec(query string, args ...any) (sql.Result, error)
	Query(query string, args ...any) (*sql.Rows, error)
}

type ICursor interface {
	Exec(query string, args ...any) error
	ExecReturnId(query string, args ...any) (int64, error)
	Iter(f func(*sql.Rows) error, query string, args ...any) error
	Registry() IRegistry
}

type IDb interface {
	ICursor
	WithLocked(f func(ICursor) error) error
	// mutex lock-write + create transaction
	WithTxLocked(f func(ICursor) error) error
}

type ITable interface {
	CreateQuery() []string
	// добавить методы для управления миграциями
}

type ISerDe interface {
	// convert specific type to sql params
	ToParams(any) []any
	// make receivers for .Scan function
	Receivers() []any
	// convert specific type to sql params
	FromParams([]any) any
}

type IAtomic interface {
	ToParam(any) any
	Receiver() any
	FromParam(any) any
}

type IAtomicSerDe interface {
	IAtomic
	ISerDe
}

type IRegistry interface {
	GetOrCreateAny(val any) ISerDe
	GetOrCreateTp(tp reflect.Type) ISerDe
}

type IMigrationOperation interface {
	ChangeState(state *state.State) error
	Query() string
}

type IMigration interface {
	GetOperations() []IMigrationOperation
	IsFake() bool
	GetName() string
	ToString() string
}

type IMigrationManager interface {
	CreateTables(tables []any) error
	ApplyMigrations([]IMigration) error
	GenerateMigration(
		name string,
		previous []IMigration,
		tables []any,
	) (IMigration, error)
}
