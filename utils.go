package ss2

import (
	"strings"

	"gitlab.com/asvedr/ss2/internal/table_builder"
)

type TUtils struct{}

// returns columns as string: "a,b,c"
func (TUtils) GetAllColumns(table any) string {
	t := table_builder.BuildTable(table)
	return strings.Join(t.ColumnOrder, ",")
}

func (TUtils) ParamTemplate(count int) string {
	if count < 1 {
		panic("count < 0")
	}
	if count == 1 {
		return "(?)"
	}
	return "(?" + strings.Repeat(",?", count-1) + ")"
}

func (TUtils) Repeat(tmpl string, count int) string {
	if count < 1 {
		panic("count < 0")
	}
	if count == 1 {
		return tmpl
	}
	return tmpl + strings.Repeat(","+tmpl, count-1)
}

func CastToAny[T any](args []T) []any {
	res := make([]any, len(args))
	for i, arg := range args {
		res[i] = arg
	}
	return res
}
