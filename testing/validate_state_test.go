package ss2_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
	"gitlab.com/asvedr/ss2/internal/table_builder"
)

type tve1 struct {
	A    int `pk:"t"`
	Meta struct {
		Name ss2.None `value:"t"`
	}
}
type tve2 struct {
	B    int `pk:"t"`
	Meta struct {
		Name ss2.None `value:"t"`
	}
}

type tve_no_pk struct {
	A    int
	Meta struct {
		Name ss2.None `value:"t"`
	}
}

type tve_m_pk struct {
	A    int `pk:"t"`
	B    int `pk:"t"`
	Meta struct {
		Name ss2.None `value:"t"`
	}
}

type tve_dup_idx struct {
	A    int `pk:"t"`
	B    int
	Meta struct {
		Name     ss2.None `value:"t"`
		Index_ix ss2.None `value:"B"`
		Index_Ix ss2.None `value:"A,B" unique:"t"`
	}
}

type tve_ix_no_col struct {
	A    int `pk:"t"`
	Meta struct {
		Name     ss2.None `value:"t"`
		Index_ix ss2.None `value:""`
	}
}

type tve_ix_col_nf struct {
	A    int `pk:"t"`
	Meta struct {
		Name     ss2.None `value:"t"`
		Index_ix ss2.None `value:"C,D"`
	}
}

var validation_err_params = []struct {
	name   string
	tables []any
	err    string
}{
	{"duplicate name", []any{tve1{}, tve2{}}, "duplicate table name: t"},
	{"no pk", []any{tve_no_pk{}}, "table t has no primary key"},
	{"many pk", []any{tve_m_pk{}}, "table t has many primary keys"},
	{"dup idx", []any{tve_dup_idx{}}, "table t duplicate index name: ix_t_index_ix"},
	{"ix col not found", []any{tve_ix_col_nf{}}, "table t, index ix_t_index_ix column not found in table: c"},
	{"ix no col", []any{tve_ix_no_col{}}, "table t, index ix_t_index_ix has no columns"},
}

func TestStateValidationErrors(t *testing.T) {
	for _, tt := range validation_err_params {
		t.Run(tt.name, func(t *testing.T) {
			state := table_builder.BuildState(tt.tables)
			err := state.Validate()
			if err == nil {
				t.Fatal("expected error")
			}
			exp := "Inconsistent migration state: " + tt.err
			assert.Equal(t, exp, err.Error())
		})
	}
}
