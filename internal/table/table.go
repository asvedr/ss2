package table

import (
	"fmt"
	"reflect"
	"strings"
)

type Table struct {
	Name        string
	Columns     map[string]Column
	ColumnOrder []string
	Indexes     []Index
}

type ForeignKey struct {
	Table  string
	Column string
}

func (self ForeignKey) String() string {
	return self.Table + "." + self.Column
}

type Column struct {
	Name       string
	Type       string
	PrimaryKey bool
	ForeignKey *ForeignKey
	NotNull    bool
	Default    *string
}

type Index struct {
	Name    string
	Columns []string
	Unique  bool
}

type MetaField struct {
	TableName string
	Indexes   []Index
}

func (self Column) DeclareQuery() string {
	query := self.Name + ` ` + self.Type
	if self.PrimaryKey {
		query += ` primary key`
	}
	if self.Default != nil {
		query += ` DEFAULT ` + *self.Default
	}
	if self.NotNull {
		query += ` not null`
	}
	return query
}

func (self Column) DeclareForeignKey() string {
	return fmt.Sprintf(
		"\n,FOREIGN KEY (%s) REFERENCES %s(%s)",
		self.Name,
		self.ForeignKey.Table,
		self.ForeignKey.Column,
	)
}

func (self Table) CreateQuery() string {
	cr_table := `create table if not exists ` + self.Name
	cr_table += ` (`
	first := true
	fks := ""
	for _, column := range self.Columns {
		if !first {
			cr_table += `, `
		}
		first = false
		cr_table += column.DeclareQuery()
		if column.ForeignKey != nil {
			fks += column.DeclareForeignKey()
		}
	}
	if fks != "" {
		cr_table += fks
	}
	cr_table += `)`
	return cr_table
}

func CreateIndex(table string, index Index) string {
	prefix := `create `
	if index.Unique {
		prefix += `unique `
	}
	return fmt.Sprintf(
		`%sindex if not exists %s on %s(%s)`,
		prefix,
		index.Name,
		table,
		strings.Join(index.Columns, `, `),
	)
}

func (self Index) Equal(other Index) bool {
	return reflect.DeepEqual(self, other)
}
