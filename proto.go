package ss2

import "gitlab.com/asvedr/ss2/internal/proto"

type ICursor interface {
	Internal() proto.ICursor
}

type IDb interface {
	ICursor
	MigrationManager() IMigrationManager
	// mutex lock
	WithLocked(f func(ICursor) error) error
	// mutex lock- + create transaction
	WithTxLocked(f func(ICursor) error) error
}

type IMigrationManager interface {
	CreateTables(tables ...any) error
	ApplyMigrations([]string) error
	GenerateMigration(
		name string,
		previous []string,
		tables ...any,
	) (string, error)
}
