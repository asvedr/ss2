package ss2_test

import (
	"testing"

	"gitlab.com/asvedr/ss2"
)

func cast[T any](t T) {}

func TestIntMigrationManager(t *testing.T) {
	cast[ss2.IMigrationManager](ss2.MigrationManager{})
}
