package ss2_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

type TableWithOptField struct {
	Id   int `pk:"t"`
	Val  *int
	Tm   *time.Time
	Meta struct {
		Name ss2.None `value:"tbl"`
	}
}

func TestOptFields(t *testing.T) {
	when := time.UnixMilli(1257894000000).UTC()
	db, err := ss2.NewDb(":memory:")
	assert.Nil(t, err)
	err = ss2.MigrationManager{Db: db}.CreateTables(TableWithOptField{})
	assert.Nil(t, err)
	err = ss2.Exec(
		db,
		"INSERT INTO tbl (id, val, tm) VALUES (?, ?, ?), (?, ?, ?)",
		1, 3, nil,
		2, nil, when,
	)
	assert.Nil(t, err)

	rows, err := ss2.FetchSlice[TableWithOptField](
		db,
		"SELECT id, val, tm FROM tbl ORDER BY id",
	)
	assert.Nil(t, err)
	assert.Equal(t, 2, len(rows))

	assert.Equal(t, 3, *rows[0].Val)
	assert.Nil(t, rows[0].Tm)

	assert.Nil(t, rows[1].Val)
	assert.Equal(t, when, *rows[1].Tm)
}

type NullTbl struct {
	Name     string
	Required int
	Opt      *int
	Time     time.Time
	Msg      string
}

func TestInsertNull(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)
	err = ss2.MigrationManager{Db: db}.CreateTables(NullTbl{})
	assert.NoError(t, err)
	var name, msg string
	var required int
	var opt *int
	var _time int64
	name = "name"
	required = -123
	_time = 1577836800
	msg = "msg"
	ss2.Exec(
		db,
		"INSERT INTO nulltbl (name, required, opt, time, msg) VALUES (?, ?, ?, ?, ?)",
		name,
		required,
		opt,
		_time,
		msg,
	)
	assert.NoError(t, err)

	obj, err := ss2.FetchOne[NullTbl](
		db,
		"SELECT name, required, opt, time, msg FROM nulltbl",
	)
	assert.NoError(t, err)
	assert.Equal(
		t,
		NullTbl{
			Name:     name,
			Required: required,
			Opt:      opt,
			Time:     time.UnixMilli(_time).UTC(),
			Msg:      msg,
		},
		obj,
	)
}
