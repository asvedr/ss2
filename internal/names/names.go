package names

const (
	FieldMeta = "Meta"
	// meta field "table name"
	FieldTableName = "Name"
	// meta field index prefix
	FieldIndexPrefix = "Index_"
)

const (
	// value of meta field
	TagValue = "value"
	// uniq index or uniq field flag
	TagUnique = "unique"
	// index field flag
	TagIndex = "index"
	// alias for field
	TagName = "name"
	// primary key flag
	TagPrimaryKey = "pk"
	// foreign key field
	TagForeignKey = "fk"
	// default value due migration
	TagDefault = "db_default"
)
