package ss2

import "gitlab.com/asvedr/ss2/internal/proto"

type cursor_wrapper struct {
	proto.ICursor
}

func (self cursor_wrapper) Internal() proto.ICursor {
	return self
}
