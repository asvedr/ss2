package migration

import (
	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type RedefineColumn struct {
	Table  string
	Column table.Column
}

func (self RedefineColumn) ChangeState(state *state.State) error {
	tbl := state.Tables[self.Table]
	tbl.Columns[self.Column.Name] = self.Column
	return nil
}

func (self RedefineColumn) Query() string {
	return ""
}
