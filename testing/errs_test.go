package ss2_test

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
	"modernc.org/sqlite"
)

func TestWrapErrDefault(t *testing.T) {
	db := setup()
	ss2.MigrationManager{Db: db}.CreateTables(Tbl{})
	err := ss2.Exec(db, "insert into tbl (key, val) values (?, ?)", "a", "b")
	assert.Nil(t, err)

	custom_err := errors.New("custom")

	err = ss2.Iter(
		db,
		func(row Tbl) error { return custom_err },
		"select id, key, val from tbl",
	)
	assert.Equal(t, err, custom_err)
}

func TestWrapErrUnknownSqliteErr(t *testing.T) {
	db := setup()
	ss2.MigrationManager{Db: db}.CreateTables(Tbl{})
	err := ss2.Exec(db, "insert into tbl (key, val) values (?, ?)", "a", "b")
	assert.Nil(t, err)

	_, err = ss2.FetchSlice[int](
		db,
		"select bad_field from tbl",
	)
	_, casted := err.(*sqlite.Error)
	assert.True(t, casted)
}
