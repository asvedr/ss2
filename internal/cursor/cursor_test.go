package cursor_test

import (
	"database/sql"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2/internal/cursor"
	"gitlab.com/asvedr/ss2/mocks"
	_ "modernc.org/sqlite"
)

var err_custom = errors.New("custom")

func TestRegistry(t *testing.T) {
	assert.Nil(t, cursor.New(nil, nil).Registry())
}

func TestExec(t *testing.T) {
	exec := &mocks.Executor{}
	exec.On("Exec", "query", []any{1}).Return(nil, err_custom)
	err := cursor.New(nil, exec).Exec("query", 1)
	assert.Equal(t, err, err_custom)
}

func TestExecReturnIdOk(t *testing.T) {
	exec := &mocks.Executor{}
	res := &mocks.SQLResult{}
	exec.On("Exec", "query", []any{1}).Return(res, nil)
	res.On("LastInsertId").Return(int64(123), nil)
	id, err := cursor.New(nil, exec).ExecReturnId("query", 1)
	assert.Nil(t, err)
	assert.Equal(t, int64(123), id)
}

func TestExecReturnIdErr(t *testing.T) {
	exec := &mocks.Executor{}
	exec.On("Exec", "query", []any{1}).Return(nil, err_custom)
	_, err := cursor.New(nil, exec).ExecReturnId("query", 1)
	assert.Equal(t, err, err_custom)
}

func TestIterOk(t *testing.T) {
	exec := &mocks.Executor{}
	db, err := sql.Open("sqlite", ":memory:")
	assert.Nil(t, err)
	rows, err := db.Query("select 1")
	assert.Nil(t, err)
	exec.On("Query", "query", []any{1}).Return(rows, nil)
	f := func(r *sql.Rows) error {
		return nil
	}
	err = cursor.New(nil, exec).Iter(f, "query", 1)
	assert.Nil(t, err)
}

func TestIterCustomErr(t *testing.T) {
	exec := &mocks.Executor{}
	db, err := sql.Open("sqlite", ":memory:")
	assert.Nil(t, err)
	rows, err := db.Query("select 1")
	assert.Nil(t, err)
	exec.On("Query", "query", []any{1}).Return(rows, nil)
	f := func(r *sql.Rows) error {
		return err_custom
	}
	err = cursor.New(nil, exec).Iter(f, "query", 1)
	assert.Equal(t, err, err_custom)
}

func TestIterErr(t *testing.T) {
	exec := &mocks.Executor{}
	exec.On("Query", "query", []any{1}).Return(nil, err_custom)
	f := func(r *sql.Rows) error {
		return nil
	}
	err := cursor.New(nil, exec).Iter(f, "query", 1)
	assert.Equal(t, err, err_custom)
}
