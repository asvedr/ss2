package migration

import (
	"slices"

	"gitlab.com/asvedr/ss2/internal/state"
)

type DropTable struct {
	Table string
}

func (self DropTable) ChangeState(state *state.State) error {
	delete(state.Tables, self.Table)
	state.CreateOrder = slices.DeleteFunc(
		state.CreateOrder,
		func(x string) bool { return x == self.Table },
	)
	return nil
}

func (self DropTable) Query() string {
	return `DROP TABLE IF EXISTS ` + self.Table
}
