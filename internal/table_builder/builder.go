package table_builder

import (
	"fmt"
	"reflect"
	"strings"

	"gitlab.com/asvedr/ss2/internal/names"
	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/stdtp"
	"gitlab.com/asvedr/ss2/internal/table"
)

func BuildState(src []any) *state.State {
	tables := make(map[string]table.Table)
	order := make([]string, len(src))
	for i, strct := range src {
		tbl := BuildTable(strct)
		tables[tbl.Name] = tbl
		order[i] = tbl.Name
	}
	return &state.State{Tables: tables, CreateOrder: order}
}

func BuildTable(t any) table.Table {
	tp := reflect.TypeOf(t)
	if tp.Kind() != reflect.Struct {
		panic("FromStruct arg must be struct")
	}
	columns, column_order, indexes := build_columns(tp)
	meta_field := get_meta_field(tp)
	indexes = append(indexes, meta_field.Indexes...)
	table := table.Table{
		Name:        strings.ToLower(meta_field.TableName),
		Columns:     columns,
		ColumnOrder: column_order,
		Indexes:     indexes,
	}
	for i, index := range table.Indexes {
		name := fmt.Sprintf("ix_%s_%s", table.Name, index.Name)
		index.Name = name
		table.Indexes[i] = index
	}
	return table
}

func build_columns(
	tp reflect.Type,
) (map[string]table.Column, []string, []table.Index) {
	columns := map[string]table.Column{}
	column_order := []string{}
	var indexes []table.Index
	for i := 0; i < tp.NumField(); i++ {
		field := tp.Field(i)
		if strings.HasPrefix(field.Name, names.FieldMeta) {
			continue
		}
		column, opt_index := build_column(field)
		if opt_index != nil {
			indexes = append(indexes, *opt_index)
		}
		columns[column.Name] = column
		column_order = append(column_order, column.Name)
	}
	return columns, column_order, indexes
}

func build_column(field reflect.StructField) (table.Column, *table.Index) {
	name, found := field.Tag.Lookup(names.TagName)
	if !found {
		name = field.Name
	}
	name = strings.ToLower(name)
	_, primary_key := field.Tag.Lookup(names.TagPrimaryKey)
	sql_type, not_null := build_type(field.Type)
	column := table.Column{
		Name:       name,
		Type:       sql_type,
		PrimaryKey: primary_key,
		ForeignKey: build_foreign_key(field),
		NotNull:    not_null,
		Default:    prepare_column_default(field),
	}
	index := prepare_field_index(name, field)
	return column, index
}

func build_foreign_key(field reflect.StructField) *table.ForeignKey {
	tag, found := field.Tag.Lookup(names.TagForeignKey)
	if !found {
		return nil
	}
	split := strings.Split(strings.ToLower(tag), ".")
	if len(split) != 2 {
		panic(`Bad foreign key tag: "` + tag + `"`)
	}
	return &table.ForeignKey{
		Table:  split[0],
		Column: split[1],
	}
}

func prepare_column_default(field reflect.StructField) *string {
	value, found := field.Tag.Lookup(names.TagDefault)
	if found {
		return &value
	}
	return nil
}

func prepare_field_index(name string, field reflect.StructField) *table.Index {
	_, is_uniq := field.Tag.Lookup(names.TagUnique)
	if is_uniq {
		return &table.Index{
			Name:    strings.ToLower("_autofield_" + name + "_uniq"),
			Columns: []string{name},
			Unique:  true,
		}
	}
	_, is_indexed := field.Tag.Lookup(names.TagIndex)
	if is_indexed {
		return &table.Index{
			Name:    strings.ToLower("_autofield_" + name + "_ix"),
			Columns: []string{name},
			Unique:  false,
		}
	}
	return nil
}

func build_type(tp reflect.Type) (string, bool) {
	if tp.Kind() == reflect.Pointer {
		return stdtp.GoTpToSqlTp[tp.Elem()], false
	}
	return stdtp.GoTpToSqlTp[tp], true
}

func get_meta_field(tp reflect.Type) table.MetaField {
	field, found := tp.FieldByName(names.FieldMeta)
	if !found {
		return table.MetaField{
			TableName: default_name(tp),
		}
	}
	return make_meta_field(tp, field.Type)
}

func default_name(table reflect.Type) string {
	return strings.ToLower(table.Name())
}

func make_meta_field(tbl_tp, field reflect.Type) table.MetaField {
	name_field, found := field.FieldByName(names.FieldTableName)
	var name string
	if found {
		name = get_value(tbl_tp, true, name_field)
	} else {
		name = default_name(tbl_tp)
	}
	var indexes []table.Index
	for i := 0; i < field.NumField(); i++ {
		field := field.Field(i)
		if strings.HasPrefix(field.Name, names.FieldIndexPrefix) {
			indexes = append(indexes, build_index(tbl_tp, field))
		}
	}
	return table.MetaField{TableName: name, Indexes: indexes}
}

func build_index(tbl_tp reflect.Type, field reflect.StructField) table.Index {
	_, unique := field.Tag.Lookup(names.TagUnique)
	split := strings.Split(get_value(tbl_tp, true, field), ",")
	columns := []string{}
	for _, column := range split {
		column := strings.TrimSpace(column)
		if column == "" {
			continue
		}
		columns = append(columns, strings.ToLower(column))
	}
	return table.Index{
		Name:    strings.ToLower(field.Name),
		Columns: columns,
		Unique:  unique,
	}
}

func get_value(strct reflect.Type, is_in_meta bool, field reflect.StructField) string {
	value, found := field.Tag.Lookup(names.TagValue)
	if !found {
		var msg string
		if is_in_meta {
			msg = strct.String() + ".Meta." + field.Name + " has no value"
		} else {
			msg = strct.String() + "." + field.Name + " has no value"
		}
		panic(msg)
	}
	return value
}
