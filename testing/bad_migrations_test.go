package ss2_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2/internal/migration"
)

const add_column_no_column = `
name: add_column_no_column
operations: [{type: add_column, table: t}]
`

const add_column_no_tbl = `
name: add_column_no_tbl
operations:
    - type: add_column
      new_column: {name: field, type: real}
`

const bad_type = `
name: bad_type
operations: [{type: "xxx"}]
`

const redefine_no_tbl = `
name: redefine_no_tbl
operations:
    - type: redefine_column
      new_column: {name: col, type: integer}
`

const redefine_no_column = `
name: redefine_no_column
operations: [{type: redefine_column, table: t1}]
`
const add_index_no_tbl = `
name: add_index_no_tbl
operations: [{type: add_index, name: ix, columns: [fint]}]
`
const add_index_no_columns = `
name: add_index_no_columns
operations: [{type: add_index, table: t2, name: ix}]
`
const add_index_no_name = `
name: add_index_no_name
operations:
    - {type: add_index, table: t2, columns: [fint]}
`
const add_table_no_table = `
name: add_table_no_table
operations:
    - type: add_table
      new_columns: [{"name": "id", "type": "integer"}]
`

const sql_no_sql = `
name: sql
operations: [{type: sql}]
`

const drop_column_no_tbl = `
name: drop_column_no_tbl
operations: [{type: drop_column, column: col}]
`
const drop_column_no_column = `
name: drop_column_no_column
operations: [{type: drop_column, table: t}]
`
const drop_index_no_index = `
name: drop_index
operations: [{type: drop_index, table: t2}]
`
const drop_index_no_tbl = `
name: drop_index
operations: [{type: drop_index, index: ix}]
`
const drop_table_no_tbl = `
name: drop_table
operations: [{type: drop_table}]
`

var mig_to_err = map[string]string{
	bad_type:              "invalid operation type",
	add_column_no_column:  "missing new column",
	add_column_no_tbl:     "missing table name",
	redefine_no_tbl:       "missing table name",
	redefine_no_column:    "missing new column",
	add_index_no_tbl:      "missing table name",
	add_index_no_columns:  "missing columns",
	add_index_no_name:     "missing index name",
	add_table_no_table:    "missing table name",
	sql_no_sql:            "missing sql",
	drop_table_no_tbl:     "missing table name",
	drop_index_no_index:   "missing index name",
	drop_index_no_tbl:     "missing table name",
	drop_column_no_tbl:    "missing table name",
	drop_column_no_column: "missing column name",
}

func TestBadMigrations(t *testing.T) {
	for src, msg := range mig_to_err {
		t.Run(src, func(t *testing.T) {
			_, err := migration.FromString(src)
			if err == nil {
				t.Fatal("expected error")
			}
			assert.Equal(t, msg, err.Error())
		})
	}
}
