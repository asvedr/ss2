package ss2

import (
	"gitlab.com/asvedr/ss2/internal/db"
	"gitlab.com/asvedr/ss2/internal/proto"
)

type Db struct{ db.Db }

type None struct{}

var Utils TUtils

func NewDb(path string) (IDb, error) {
	inner, err := db.New(path)
	if err != nil {
		return nil, err
	}
	err = inner.Exec("PRAGMA foreign_keys = ON")
	return Db{Db: inner}, err
}

func (self Db) MigrationManager() IMigrationManager {
	return MigrationManager{Db: self}
}

func (self Db) Internal() proto.ICursor {
	return self.Db
}

func (self Db) WithLocked(f func(ICursor) error) error {
	return self.Db.WithLocked(func(cursor proto.ICursor) error {
		return f(cursor_wrapper{cursor})
	})
}

func (self Db) WithTxLocked(f func(ICursor) error) error {
	return self.Db.WithTxLocked(func(cursor proto.ICursor) error {
		return f(cursor_wrapper{cursor})
	})
}
