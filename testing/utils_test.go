package ss2_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

func TestToAny(t *testing.T) {
	assert.Equal(
		t,
		[]any{1, 2, 3},
		ss2.CastToAny([]int{1, 2, 3}),
	)
}

func TestGetAllColumns(t *testing.T) {
	assert.Equal(
		t,
		"id,key,val",
		ss2.Utils.GetAllColumns(Tbl{}),
	)
	assert.Equal(
		t,
		"a",
		ss2.Utils.GetAllColumns(tve1{}),
	)
}

func TestParamTemplate(t *testing.T) {
	assert.Equal(
		t, "(?)", ss2.Utils.ParamTemplate(1),
	)
	assert.Equal(
		t,
		"(?,?,?)",
		ss2.Utils.ParamTemplate(3),
	)
	assert.Panics(t, func() { ss2.Utils.ParamTemplate(0) })
}

func TestRepeat(t *testing.T) {
	assert.Equal(
		t, "ab", ss2.Utils.Repeat("ab", 1),
	)
	assert.Equal(
		t,
		"ab,ab,ab",
		ss2.Utils.Repeat("ab", 3),
	)
	assert.Panics(t, func() { ss2.Utils.Repeat("ab", 0) })
}
