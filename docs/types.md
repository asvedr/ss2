# Available standart types for SQL conversion
signed int types

`int/int8/int16/int32/int64` => `integer`

unsigned int types

`uint/uint8/uint16/uint32/uint64` => `integer`

time is converted as timestamp (millis)

`time.Time` => `integer`

real types

`float32/float64` => `real`

boolean type

`bool` => `boolean`

string type

`string` => `text`

**POINTER TO A STD TYPE IS INTERPRETED AS A NULLABLE VALUE**

```go
type T struct {
    f1 time.Time
    f2 *int
}
// select rows where f1 is not null value and f2 is nullable value
rows, err := ss2.FetchOne[T]("select ...")
```

[Type serialization docs](/docs/serialization.md)
[Table or struct declaration](/docs/structs_and_tables.md)