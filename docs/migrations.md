# How to init db without migration

You must declare tables at first. [Table docs](/docs/structs_and_tables.md#table-declaration)

```go
err := ss2.MigrationManager{Db: db}.CreateTables(
    Tbl1{},
    Tbl2{},
    ...
)
```

# How to generate migrations
```go
// in this case explicit db declaration is optional
// db, _ := ss2.NewDb(":memory:")
// mm := ss2.MigrationManager{Db: db}
mm := ss2.MigrationManager{}

var previous_migrations []string
var mig string
mig, err := mm.GenerateMigration(
    previous_migrations,
    Tbl1{}, Tbl2{}, ...
)
```
If no changes are detected then `mig == ""`

# How to apply migrations
```go
db, _ := ss2.NewDb(":memory:")
mm := ss2.MigrationManager{Db: db}
var migrations []string
err := mm.ApplyMigrations(migrations)
```