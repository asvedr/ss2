package registry

import (
	"reflect"

	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/serde"
	"gitlab.com/asvedr/ss2/internal/stdtp"
)

type Registry struct {
	storage map[reflect.Type]proto.ISerDe
}

func New() proto.IRegistry {
	storage := map[reflect.Type]proto.ISerDe{}
	for key, val := range stdtp.Map {
		storage[key] = val
	}

	return Registry{storage: storage}
}

func (self Registry) GetOrCreateAny(val any) proto.ISerDe {
	tp := reflect.TypeOf(val)
	if val == nil {
		return stdtp.NilSerde
	}
	return self.GetOrCreateTp(tp)
}

func (self Registry) GetOrCreateTp(tp reflect.Type) proto.ISerDe {
	res := self.storage[tp]
	if res != nil {
		return res
	}
	res = serde.NewTp(tp)
	self.storage[tp] = res
	return res
}
