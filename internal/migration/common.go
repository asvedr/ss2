package migration

func bool2opt(val bool) *bool {
	if val {
		return &val
	}
	return nil
}

func opt2bool(val *bool) bool {
	if val == nil {
		return false
	}
	return *val
}
