package stdtp

import "reflect"

func tp_name[T any]() reflect.Type {
	var t T
	return reflect.TypeOf(t)
}
