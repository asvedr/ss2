package migration

import (
	"gitlab.com/asvedr/ss2/internal/proto"
	"gopkg.in/yaml.v3"
)

type Migration struct {
	Name       string
	Fake       bool
	Operations []proto.IMigrationOperation
}

func (self Migration) GetName() string {
	return self.Name
}

func (self Migration) IsFake() bool {
	return self.Fake
}

func (self Migration) GetOperations() []proto.IMigrationOperation {
	return self.Operations
}

func (self Migration) ToString() string {
	operations := make([]YamlOperation, len(self.Operations))
	var err error
	for i, op := range self.Operations {
		operations[i], err = operation2yaml(op)
		if err != nil {
			panic(err)
		}
	}
	bts, err := yaml.Marshal(YamlMigration{
		Name:       self.Name,
		Fake:       bool2opt(self.Fake),
		Operations: operations,
	})
	if err != nil {
		panic(err)
	}
	return string(bts)
}

func FromString(src string) (Migration, error) {
	buffer := YamlMigration{}
	err := yaml.Unmarshal([]byte(src), &buffer)
	if err != nil {
		return Migration{}, err
	}
	operations := make([]proto.IMigrationOperation, len(buffer.Operations))
	for i, op := range buffer.Operations {
		operations[i], err = op.Compile()
		if err != nil {
			return Migration{}, err
		}
	}
	return Migration{
		Name:       buffer.Name,
		Fake:       opt2bool(buffer.Fake),
		Operations: operations,
	}, nil
}
