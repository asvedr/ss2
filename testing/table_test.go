package ss2_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
	"gitlab.com/asvedr/ss2/internal/table_builder"
)

type TableWithFk struct {
	Id int `pk:"t"`
	Fk int `fk:"invalid_value"`
}

func TestTableBuildPanic(t *testing.T) {
	assert.Panics(t, func() { table_builder.BuildTable(123) })

	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)

	assert.Panics(t, func() {
		db.MigrationManager().CreateTables(TableWithFk{})
	})
}
