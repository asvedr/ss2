package queries

import (
	"database/sql"
	"reflect"

	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/proto"
)

func get_or_create[T any](reg proto.IRegistry) proto.ISerDe {
	var t T
	return reg.GetOrCreateTp(reflect.TypeOf(t))
}

func prepare_params(reg proto.IRegistry, params []any) []any {
	args := []any{}
	for _, param := range params {
		p_serde := reg.GetOrCreateAny(param)
		args = append(args, p_serde.ToParams(param)...)
	}
	return args
}

func Exec(
	db proto.ICursor,
	query string,
	params []any,
) error {
	args := prepare_params(db.Registry(), params)
	return db.Exec(query, args...)
}

func ExecReturnId(
	db proto.ICursor,
	query string,
	params []any,
) (int64, error) {
	args := prepare_params(db.Registry(), params)
	id, err := db.ExecReturnId(query, args...)
	return id, err
}

func FetchOne[T any](
	db proto.ICursor,
	query string,
	params []any,
) (T, error) {
	reg := db.Registry()
	var result T
	var err error
	var found bool
	serde := get_or_create[T](reg)
	f := func(r *sql.Rows) error {
		vars := serde.Receivers()
		err := r.Scan(vars...)
		if err != nil {
			return err
		}
		result = serde.FromParams(vars).(T)
		found = true
		return err
	}
	args := prepare_params(reg, params)
	err = db.Iter(f, query, args...)
	if err != nil {
		return result, err
	}
	if !found {
		return result, errs.ErrNotFound{}
	}
	return result, nil
}

func FetchOpt[T any](
	db proto.ICursor,
	query string,
	params []any,
) (*T, error) {
	reg := db.Registry()
	var result T
	var err error
	var found bool
	serde := get_or_create[T](reg)
	f := func(r *sql.Rows) error {
		vars := serde.Receivers()
		err := r.Scan(vars...)
		if err != nil {
			return err
		}
		result = serde.FromParams(vars).(T)
		found = true
		return err
	}
	args := prepare_params(reg, params)
	err = db.Iter(f, query, args...)
	if err != nil {
		return nil, err
	}
	if !found {
		return nil, nil
	}
	return &result, nil
}

func FetchSlice[T any](
	db proto.ICursor,
	query string,
	params []any,
) ([]T, error) {
	reg := db.Registry()
	var result []T
	var err error
	serde := get_or_create[T](reg)
	f := func(r *sql.Rows) error {
		vars := serde.Receivers()
		err := r.Scan(vars...)
		if err != nil {
			return err
		}
		item := serde.FromParams(vars).(T)
		result = append(result, item)
		return err
	}
	args := prepare_params(reg, params)
	err = db.Iter(f, query, args...)
	return result, err
}

func Iter[T any](
	db proto.ICursor,
	iter_func func(T) error,
	query string,
	params []any,
) error {
	reg := db.Registry()
	var err error
	serde := get_or_create[T](reg)
	f := func(r *sql.Rows) error {
		vars := serde.Receivers()
		err := r.Scan(vars...)
		if err != nil {
			return err
		}
		item := serde.FromParams(vars).(T)
		return iter_func(item)
	}
	args := prepare_params(reg, params)
	err = db.Iter(f, query, args...)
	return err
}

func FetchMap[K comparable, T any](
	db proto.ICursor,
	query string,
	params []any,
) (map[K]T, error) {
	reg := db.Registry()
	result := make(map[K]T)
	serde_key := get_or_create[K](reg)
	serde_val := get_or_create[T](reg)
	key_count := len(serde_key.Receivers())
	f := func(r *sql.Rows) error {
		recv := serde_key.Receivers()
		recv = append(recv, serde_val.Receivers()...)
		err := r.Scan(recv...)
		if err != nil {
			return err
		}
		key := serde_key.FromParams(recv[:key_count]).(K)
		val := serde_val.FromParams(recv[key_count:]).(T)
		result[key] = val
		return nil
	}
	args := prepare_params(reg, params)
	err := db.Iter(f, query, args...)
	return result, err
}
