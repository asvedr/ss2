package ss2

import (
	"gitlab.com/asvedr/ss2/internal/queries"
)

func Exec(
	db ICursor,
	query string,
	params ...any,
) error {
	return wrap_err(queries.Exec(db.Internal(), query, params))
}

func ExecReturnId(
	db ICursor,
	query string,
	params ...any,
) (int64, error) {
	id, err := queries.ExecReturnId(db.Internal(), query, params)
	return id, wrap_err(err)
}

func FetchOne[T any](
	db ICursor,
	query string,
	params ...any,
) (T, error) {
	res, err := queries.FetchOne[T](db.Internal(), query, params)
	return res, wrap_err(err)
}

func FetchOpt[T any](
	db ICursor,
	query string,
	params ...any,
) (*T, error) {
	res, err := queries.FetchOpt[T](db.Internal(), query, params)
	return res, wrap_err(err)
}

func FetchSlice[T any](
	db ICursor,
	query string,
	params ...any,
) ([]T, error) {
	res, err := queries.FetchSlice[T](db.Internal(), query, params)
	return res, wrap_err(err)
}

func Iter[T any](
	db ICursor,
	iter_func func(T) error,
	query string,
	params ...any,
) error {
	return wrap_err(queries.Iter(db.Internal(), iter_func, query, params))
}

func FetchMap[K comparable, T any](
	db ICursor,
	query string,
	params ...any,
) (map[K]T, error) {
	res, err := queries.FetchMap[K, T](db.Internal(), query, params)
	return res, wrap_err(err)
}
