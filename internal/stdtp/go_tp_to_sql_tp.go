package stdtp

import (
	"reflect"
	"time"
)

var GoTpToSqlTp = map[reflect.Type]string{
	tp_name[int]():        "integer",
	tp_name[*int]():       "integer",
	tp_name[int8]():       "integer",
	tp_name[*int8]():      "integer",
	tp_name[int16]():      "integer",
	tp_name[*int16]():     "integer",
	tp_name[int32]():      "integer",
	tp_name[*int32]():     "integer",
	tp_name[int64]():      "integer",
	tp_name[*int64]():     "integer",
	tp_name[uint]():       "integer",
	tp_name[*uint]():      "integer",
	tp_name[uint8]():      "integer",
	tp_name[*uint8]():     "integer",
	tp_name[uint16]():     "integer",
	tp_name[*uint16]():    "integer",
	tp_name[uint32]():     "integer",
	tp_name[*uint32]():    "integer",
	tp_name[uint64]():     "integer",
	tp_name[*uint64]():    "integer",
	tp_name[time.Time]():  "integer",
	tp_name[*time.Time](): "integer",
	tp_name[float32]():    "real",
	tp_name[*float32]():   "real",
	tp_name[float64]():    "real",
	tp_name[*float64]():   "real",
	tp_name[bool]():       "boolean",
	tp_name[*bool]():      "boolean",
	tp_name[string]():     "text",
	tp_name[*string]():    "text",
}
