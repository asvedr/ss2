package migration

import (
	"fmt"

	"gitlab.com/asvedr/ss2/internal/state"
)

type DropColumn struct {
	Table  string
	Column string
}

func (self DropColumn) ChangeState(state *state.State) error {
	tbl := state.Tables[self.Table]
	_, found := tbl.Columns[self.Column]
	if !found {
		return fmt.Errorf(
			"can not find column %s:%s",
			self.Table,
			self.Column,
		)
	}
	delete(tbl.Columns, self.Column)
	return nil
}

func (self DropColumn) Query() string {
	return fmt.Sprintf(
		`ALTER TABLE %s DROP COLUMN %s`,
		self.Table,
		self.Column,
	)
}
