package mocks

import (
	"database/sql"

	"github.com/stretchr/testify/mock"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/registry"
)

type Cursor struct {
	mock.Mock
	reg proto.IRegistry
}

var _ proto.ICursor = &Cursor{}

func (self *Cursor) Exec(query string, args ...any) error {
	any_args := []any{query}
	any_args = append(any_args, args...)
	return self.Called(any_args...).Error(0)
}

func (self *Cursor) ExecReturnId(query string, args ...any) (int64, error) {
	any_args := []any{query}
	any_args = append(any_args, args...)
	called := self.Called(any_args...)
	return int64(called.Int(0)), called.Error(1)
}

func (self *Cursor) Iter(f func(*sql.Rows) error, query string, args ...any) error {
	any_args := []any{query}
	any_args = append(any_args, args...)
	return self.Called(any_args...).Error(0)
}

func (self *Cursor) Registry() proto.IRegistry {
	if self.reg == nil {
		self.reg = registry.New()
	}
	return self.reg
}
