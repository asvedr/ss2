package mocks

import "github.com/stretchr/testify/mock"

type SQLResult struct {
	mock.Mock
}

func (self *SQLResult) LastInsertId() (int64, error) {
	called := self.Called()
	return called.Get(0).(int64), called.Error(1)
}

func (self *SQLResult) RowsAffected() (int64, error) {
	called := self.Called()
	return called.Get(0).(int64), called.Error(1)
}
