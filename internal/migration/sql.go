package migration

import "gitlab.com/asvedr/ss2/internal/state"

type Sql struct {
	Sql string
}

func (self Sql) ChangeState(state *state.State) error {
	return nil
}

func (self Sql) Query() string {
	return self.Sql
}
