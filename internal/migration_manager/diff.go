package migration_manager

import (
	"reflect"
	"slices"

	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/migration"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type diff_maker struct {
	old_state      *state.State
	new_state      *state.State
	allow_redefine bool
}

func (self diff_maker) make_diff() ([]proto.IMigrationOperation, error) {
	res := self.drop_unused_tables()
	res = append(res, self.add_new_tables()...)
	alters, err := self.alter_tables()
	res = append(res, alters...)
	return res, err
}

func (self diff_maker) drop_unused_tables() []proto.IMigrationOperation {
	var res []proto.IMigrationOperation
	for _, old_tbl_name := range self.old_state.RevOrder() {
		old_tbl := self.old_state.Tables[old_tbl_name]
		_, found := self.new_state.Tables[old_tbl.Name]
		if !found {
			res = append(res, migration.DropTable{
				Table: old_tbl.Name,
			})
			continue
		}
	}
	return res
}

func (self diff_maker) add_new_tables() []proto.IMigrationOperation {
	var res []proto.IMigrationOperation
	for _, new_tbl_name := range self.new_state.CreateOrder {
		new_tbl := self.new_state.Tables[new_tbl_name]
		_, found := self.old_state.Tables[new_tbl.Name]
		if found {
			continue
		}
		res = append(res, migration.AddTable{
			Table: new_tbl,
		})
		for _, idx := range new_tbl.Indexes {
			opr := migration.AddIndex{
				Table: new_tbl.Name,
				Index: idx,
			}
			res = append(res, opr)
		}
	}
	return res
}

func (self diff_maker) alter_tables() ([]proto.IMigrationOperation, error) {
	var res []proto.IMigrationOperation
	for _, tbl_name := range self.new_state.CreateOrder {
		old_tbl, found := self.old_state.Tables[tbl_name]
		if !found {
			continue
		}
		new_tbl := self.new_state.Tables[tbl_name]
		oprs, err := self.make_table_diff(old_tbl, new_tbl)
		if err != nil {
			return nil, err
		}
		res = append(res, oprs...)
	}
	return res, nil
}

func (self diff_maker) make_table_diff(
	old_tbl, new_tbl table.Table,
) ([]proto.IMigrationOperation, error) {
	res := []proto.IMigrationOperation{}
	for _, old_col := range old_tbl.Columns {
		new_col, found := new_tbl.Columns[old_col.Name]
		if !found {
			opr := migration.DropColumn{
				Table:  old_tbl.Name,
				Column: old_col.Name,
			}
			res = append(res, opr)
			continue
		}
		oprs, err := self.make_column_diff(
			new_tbl.Name,
			old_col,
			new_col,
		)
		if err != nil {
			return nil, err
		}
		res = append(res, oprs...)
	}
	for _, new_col := range new_tbl.Columns {
		_, found := old_tbl.Columns[new_col.Name]
		if !found {
			opr := migration.AddColumn{
				Table:  new_tbl.Name,
				Column: new_col,
			}
			err := opr.Validate()
			if err != nil {
				return nil, err
			}
			res = append(res, opr)
		}
	}
	for _, idx := range self.find_idx_to_drop(old_tbl, new_tbl) {
		mig := migration.DropIndex{
			Table: old_tbl.Name,
			Index: idx.Name,
		}
		res = append(res, mig)
	}
	for _, idx := range self.find_idx_to_add(old_tbl, new_tbl) {
		mig := migration.AddIndex{
			Table: new_tbl.Name,
			Index: idx,
		}
		res = append(res, mig)
	}
	return res, nil
}

func (self diff_maker) find_idx_to_drop(
	old_tbl, new_tbl table.Table,
) []table.Index {
	var res []table.Index
	for _, old_idx := range old_tbl.Indexes {
		if !slices.ContainsFunc(new_tbl.Indexes, old_idx.Equal) {
			res = append(res, old_idx)
		}
	}
	return res
}

func (self diff_maker) find_idx_to_add(
	old_tbl, new_tbl table.Table,
) []table.Index {
	var res []table.Index
	for _, new_idx := range new_tbl.Indexes {
		if !slices.ContainsFunc(old_tbl.Indexes, new_idx.Equal) {
			res = append(res, new_idx)
		}
	}
	return res
}

func (self diff_maker) make_column_diff(
	tbl string,
	old_col, new_col table.Column,
) ([]proto.IMigrationOperation, error) {
	if reflect.DeepEqual(old_col, new_col) {
		return nil, nil
	}
	if !self.allow_redefine {
		return nil, errs.ErrColumnChanged{
			Table:     tbl,
			OldColumn: old_col.DeclareQuery(),
			NewColumn: new_col.DeclareQuery(),
		}
	}
	opr := migration.RedefineColumn{
		Table:  tbl,
		Column: new_col,
	}
	return []proto.IMigrationOperation{opr}, nil
}
