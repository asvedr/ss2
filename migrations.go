package ss2

import (
	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/migration"
	"gitlab.com/asvedr/ss2/internal/migration_manager"
	"gitlab.com/asvedr/ss2/internal/proto"
)

type MigrationManager struct {
	Db            IDb
	AllowRedefine bool
}

func (self MigrationManager) CreateTables(tables ...any) error {
	err := self.Db.WithLocked(func(crs ICursor) error {
		mm := migration_manager.New(
			crs.Internal(),
			self.AllowRedefine,
		)
		return mm.CreateTables(tables)
	})
	return wrap_err(err)
}

func (self MigrationManager) ApplyMigrations(src []string) error {
	if len(src) == 0 {
		return nil
	}
	migrations, err := self.parse_migrations(src)
	if err != nil {
		return wrap_err(err)
	}
	err = self.Db.WithLocked(func(crs ICursor) error {
		mm := migration_manager.New(
			crs.Internal(),
			self.AllowRedefine,
		)
		return mm.ApplyMigrations(migrations)
	})
	return wrap_err(err)
}

func (self MigrationManager) GenerateMigration(
	name string,
	previous []string,
	tables ...any,
) (string, error) {
	var err error
	if self.Db == nil {
		self.Db, err = NewDb(":memory:")
		if err != nil {
			return "", wrap_err(err)
		}
	}
	migrations, err := self.parse_migrations(previous)
	if err != nil {
		return "", wrap_err(err)
	}
	var res string
	err = self.Db.WithLocked(func(crs ICursor) error {
		mm := migration_manager.New(crs.Internal(), self.AllowRedefine)
		new_m, err := mm.GenerateMigration(name, migrations, tables)
		if err != nil {
			return err
		}
		if new_m == nil {
			return nil
		}
		res = new_m.ToString()
		return nil
	})
	return res, wrap_err(err)
}

func (self MigrationManager) parse_migrations(src []string) ([]proto.IMigration, error) {
	migrations := make([]proto.IMigration, len(src))
	names := map[string]int{}
	for i, m_src := range src {
		mig, err := migration.FromString(m_src)
		if err != nil {
			return nil, err
		}
		_, used := names[mig.GetName()]
		if used {
			return nil, errs.ErrMigrationNameDuplicated{
				Name: mig.GetName(),
			}
		}
		migrations[i] = mig
		names[mig.GetName()] = i
	}
	return migrations, nil
}
