## Execute query
[how are `x` and `y` processed](/docs/serialization.md)
```go
err := ss2.Exec(
    db,
    "insert into tbl (a, b) values (?, ?)",
    x, y,
)
```
## Execute + return id of last row
```go
id, err := ss2.ExecReturnId(db, "insert into ...")
```
## Select one row
[how is `T` processed](/docs/serialization.md)
```go
// raise ss2.ErrNotFound on not found
var val T
val, err = ss2.FetchOne[T](db, "select ...")
// return nil on not found
var ptr_val *T
ptr_val, err = ss2.FetchOpt[T](db, "select ...")
```
## Fetch slice
[how is `T` processed](/docs/serialization.md)
```go
var rows []T
rows, err := ss2.FetchSlice[T](db, "select ...")
```
## Fetch dict
[how are `Key` and `Val` processed](/docs/serialization.md)
```go
var dict map[Key]Val
dict, err := ss2.FetchMap[Key, Val](db, "select ...")
```
## Iterate over rows without container creation
[how is `T` processed](/docs/serialization.md)
```go
err := ss2.Iter[T](
    db, 
    func(T) error {...}, 
    "select ...",
)
```
