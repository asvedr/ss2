package migration

import (
	"fmt"

	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
)

type AddColumn struct {
	Table  string
	Column table.Column
}

func (self AddColumn) ChangeState(state *state.State) error {
	tbl := state.Tables[self.Table]
	tbl.Columns[self.Column.Name] = self.Column
	return nil
}

func (self AddColumn) Query() string {
	query := fmt.Sprintf(
		`ALTER TABLE %s ADD COLUMN %s`,
		self.Table,
		self.Column.DeclareQuery(),
	)
	if self.Column.ForeignKey != nil {
		query += fmt.Sprintf(
			" REFERENCES %s(%s)",
			self.Column.ForeignKey.Table,
			self.Column.ForeignKey.Column,
		)
	}
	return query
}

func (self AddColumn) Validate() error {
	if self.Column.NotNull && self.Column.Default == nil {
		return fmt.Errorf(
			"table %s, column %s not null without default",
			self.Table,
			self.Column.Name,
		)
	}
	return nil
}
