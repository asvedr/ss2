package ss2_test

import "gitlab.com/asvedr/ss2"

type Tbl struct {
	Id  int `pk:"t"`
	Key string
	Val string
}

func setup() ss2.IDb {
	db, err := ss2.NewDb(":memory:")
	if err != nil {
		panic(err.Error())
	}
	err = ss2.MigrationManager{Db: db}.CreateTables(Tbl{})
	if err != nil {
		panic(err.Error())
	}
	return db
}
