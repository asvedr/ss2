package ss2_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/migration"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/state"
)

func TestIsFake(t *testing.T) {
	for _, val := range []bool{true, false} {
		t.Run(fmt.Sprint(val), func(t *testing.T) {
			m := migration.Migration{Fake: val}
			assert.Equal(t, val, m.IsFake())
		})
	}
}

type FakeOperation struct{}

func (FakeOperation) ChangeState(state *state.State) error {
	return nil
}

func (FakeOperation) Query() string {
	return ""
}

func TestToStringPanic(t *testing.T) {
	m := migration.Migration{
		Operations: []proto.IMigrationOperation{
			FakeOperation{},
		},
	}
	assert.Panics(t, func() { m.ToString() })
}

type Tbl1v1 struct {
	Id   int `pk:"t"`
	Meta struct {
		Name ss2.None `value:"t"`
	}
}

type Tbl1v2 struct {
	Id   int `pk:"t"`
	F    *string
	Meta struct {
		Name ss2.None `value:"t"`
	}
}

func TestInconsistentMigration(t *testing.T) {
	body := func(t *testing.T, ins_query string) {
		_ = `lastmigration`
		db := setup()
		mm := ss2.MigrationManager{Db: db}
		m1, err := mm.GenerateMigration("m1", nil, Tbl1v1{})
		assert.NoError(t, err)
		m2, err := mm.GenerateMigration("m2", []string{m1}, Tbl1v2{})
		assert.NoError(t, err)

		query := `CREATE TABLE lastmigration (
			name text primary key, step integer);`
		assert.NoError(t, ss2.Exec(db, query))
		assert.NoError(
			t,
			ss2.Exec(db, ins_query),
		)

		assert.ErrorAs(
			t,
			mm.ApplyMigrations([]string{m1, m2}),
			&errs.ErrInconsistentMigration{},
		)
	}
	cases := []string{
		`INSERT INTO lastmigration VALUES ('abc', 5);`,
		`INSERT INTO lastmigration VALUES ('m1', 5);`,
	}
	for _, query := range cases {
		t.Run(query, func(t *testing.T) {
			body(t, query)
		})
	}
}
