package cursor

import (
	"database/sql"

	"gitlab.com/asvedr/ss2/internal/proto"
)

type Cursor struct {
	reg proto.IRegistry
	db  proto.IExecutor
}

func New(
	reg proto.IRegistry,
	db proto.IExecutor,
) proto.ICursor {
	return Cursor{reg: reg, db: db}
}

func (self Cursor) Exec(query string, args ...any) error {
	_, err := self.db.Exec(query, args...)
	return err
}

func (self Cursor) ExecReturnId(query string, args ...any) (int64, error) {
	res, err := self.db.Exec(query, args...)
	if err != nil {
		return 0, err
	}
	return res.LastInsertId()
}

func (self Cursor) Iter(f func(*sql.Rows) error, query string, args ...any) error {
	rows, err := self.db.Query(query, args...)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		err := f(rows)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self Cursor) Registry() proto.IRegistry {
	return self.reg
}
