package migration_manager

import (
	"slices"

	"gitlab.com/asvedr/ss2/internal/errs"
	"gitlab.com/asvedr/ss2/internal/migration"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/queries"
	"gitlab.com/asvedr/ss2/internal/state"
	"gitlab.com/asvedr/ss2/internal/table"
	"gitlab.com/asvedr/ss2/internal/table_builder"
)

type manager struct {
	db             proto.ICursor
	allow_redefine bool
}

// Table
type LastMigration struct {
	Name string `pk:"t"`
	Step int
}

func New(
	db proto.ICursor,
	allow_redefine bool,
) proto.IMigrationManager {
	return manager{db: db, allow_redefine: allow_redefine}
}

func (self manager) CreateTables(structs []any) error {
	state := table_builder.BuildState(structs)
	var queries []string
	for _, name := range state.CreateOrder {
		tbl := state.Tables[name]
		queries = append(queries, tbl.CreateQuery())
		for _, idx := range tbl.Indexes {
			queries = append(
				queries,
				table.CreateIndex(tbl.Name, idx),
			)
		}
	}
	for _, query := range queries {
		if err := self.db.Exec(query); err != nil {
			return err
		}
	}
	return nil
}

func (self manager) ApplyMigrations(migrations []proto.IMigration) error {
	if len(migrations) == 0 {
		return nil
	}
	err := self.create_migration_table()
	if err != nil {
		return err
	}
	last, err := self.get_last_migration()
	if err != nil {
		return err
	}
	m_index, op_index, err := self.defer_last_migration(migrations, last)
	if err != nil {
		return err
	}
	return self.apply_with_context(migrations, m_index, op_index)
}

func (self manager) GenerateMigration(
	name string,
	previous []proto.IMigration,
	tables []any,
) (proto.IMigration, error) {
	old_state := state.New()
	for _, migration := range previous {
		if migration.GetName() == name {
			return nil, errs.ErrMigrationNameDuplicated{Name: name}
		}
		for _, op := range migration.GetOperations() {
			err := op.ChangeState(old_state)
			if err != nil {
				return nil, err
			}
		}
	}
	err := old_state.Validate()
	if err != nil {
		return nil, err
	}
	new_state := table_builder.BuildState(tables)
	err = new_state.Validate()
	if err != nil {
		return nil, err
	}
	operations, err := diff_maker{
		old_state:      old_state,
		new_state:      new_state,
		allow_redefine: self.allow_redefine,
	}.make_diff()
	if err != nil {
		return nil, err
	}
	if len(operations) == 0 {
		return nil, nil
	}
	return migration.Migration{
		Name:       name,
		Operations: operations,
	}, nil
}

func (self manager) create_migration_table() error {
	tbl := table_builder.BuildTable(LastMigration{})
	return self.db.Exec(tbl.CreateQuery())
}

func (self manager) get_last_migration() (*LastMigration, error) {
	return queries.FetchOpt[LastMigration](
		self.db,
		`SELECT name, step FROM lastmigration LIMIT 1`,
		nil,
	)
}

func (self manager) defer_last_migration(
	migrations []proto.IMigration,
	last *LastMigration,
) (int, int, error) {
	if last == nil {
		return -1, 0, nil
	}
	name := last.Name
	m_index := slices.IndexFunc(
		migrations,
		func(m proto.IMigration) bool { return m.GetName() == name },
	)
	if m_index < 0 {
		return 0, 0, last.to_err()
	}
	m := migrations[m_index]
	if last.Step > len(m.GetOperations()) {
		return 0, 0, last.to_err()
	}
	return m_index, last.Step, nil
}

func (self manager) apply_with_context(
	migrations []proto.IMigration,
	m_index int,
	op_index int,
) error {
	var err error
	var stop bool
	for {
		m_index, op_index, stop = inc_indexes(migrations, m_index, op_index)
		if stop {
			break
		}
		migration := migrations[m_index]
		opr := migration.GetOperations()[op_index]
		query := opr.Query()
		if query != "" {
			err = self.db.Exec(query)
		}
		if err != nil {
			return err
		}
		err = self.commit_step(migration, op_index)
		if err != nil {
			return err
		}
	}
	return nil
}

func (self manager) commit_step(migration proto.IMigration, op_index int) error {
	err := self.db.Exec(`DELETE FROM lastmigration`)
	if err != nil {
		return err
	}
	err = self.db.Exec(
		`INSERT INTO lastmigration (name, step) VALUES (?, ?)`,
		migration.GetName(),
		op_index,
	)
	return err
}

func (self LastMigration) to_err() errs.ErrInconsistentMigration {
	return errs.ErrInconsistentMigration{
		Name: self.Name,
		Step: self.Step,
	}
}

func inc_indexes(
	migrations []proto.IMigration,
	m_index int,
	op_index int,
) (int, int, bool) {
	if m_index < 0 {
		return 0, 0, false
	}
	migration := migrations[m_index]
	if op_index < len(migration.GetOperations())-1 {
		return m_index, op_index + 1, false
	}
	if m_index == len(migrations)-1 {
		return m_index, op_index, true
	}
	return m_index + 1, 0, false
}
