package ss2_test

import (
	"reflect"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
	"gitlab.com/asvedr/ss2/internal/stdtp"
)

type TimeTable struct {
	Id int `pk:"t"`
	Tm time.Time
}

func TestTime(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.NoError(t, err)
	err = db.MigrationManager().CreateTables(TimeTable{})
	assert.NoError(t, err)

	when := time.UnixMilli(time.Now().UnixMilli()).UTC()
	opt_time := &when

	err = ss2.Exec(
		db,
		"INSERT INTO TimeTable (tm) VALUES (?)",
		opt_time,
	)
	assert.NoError(t, err)

	res, err := ss2.FetchOne[time.Time](db, "SELECT tm FROM TimeTable")
	assert.NoError(t, err)
	assert.Equal(t, when, res)
}

func TestNilSerde(t *testing.T) {
	assert.Panics(t, func() {
		stdtp.NilSerde.Receiver()
	})
	assert.Panics(t, func() {
		stdtp.NilSerde.FromParam(123)
	})
}

func TestTimeSerde(t *testing.T) {
	serde := stdtp.Map[reflect.TypeFor[*time.Time]()]
	assert.Nil(t, serde.ToParam(nil))
	var nil_not_nil *time.Time
	assert.Nil(t, serde.ToParam(nil_not_nil))
}

func TestAsIs(t *testing.T) {
	serde := stdtp.Map[reflect.TypeFor[*int]()]
	assert.Nil(t, serde.ToParam(nil))
	var nil_not_nil *int
	assert.Nil(t, serde.ToParam(nil_not_nil))
}

func TestAsIsOpt(t *testing.T) {
	serde := stdtp.Map[reflect.TypeFor[*int]()]
	obj := 123
	assert.Equal(t, 123, serde.ToParam(&obj))
}
