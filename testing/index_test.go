package ss2_test

import (
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

type IndexedTbl struct {
	Id  int    `pk:"t"`
	Key string `unique:"t"`
	Val time.Time
}

type ManyIndexes struct {
	Id      int    `pk:"t"`
	Key     string `index:"t"`
	ValInt  int
	ValUInt uint
	ValStrA string
	ValStrB string
	Meta    struct {
		Name       ss2.None `value:"mi_tbl"`
		Index_vi   ss2.None `value:"ValInt"`
		Index_vu   ss2.None `value:"ValUInt" unique:"t"`
		Index_pair ss2.None `value:"ValStrA,ValStrB" unique:"t"`
	}
}

const expected_migration = `
name: init
operations:
    - type: add_table
      name: mi_tbl
      new_columns:
        - name: id
          type: integer
          pk: true
          not_null: true
        - name: key
          type: text
          not_null: true
        - name: valint
          type: integer
          not_null: true
        - name: valstra
          type: text
          not_null: true
        - name: valstrb
          type: text
          not_null: true
        - name: valuint
          type: integer
          not_null: true
    - type: add_index
      table: mi_tbl
      name: ix_mi_tbl__autofield_key_ix
      columns: [key]
    - type: add_index
      table: mi_tbl
      name: ix_mi_tbl_index_vi
      columns: [valint]
    - type: add_index
      table: mi_tbl
      name: ix_mi_tbl_index_vu
      columns: [valuint]
      unique: true
    - type: add_index
      table: mi_tbl
      name: ix_mi_tbl_index_pair
      columns: [valstra, valstrb]
      unique: true`

func TestCheckIndexQuerues(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.Nil(t, err)
	mm := ss2.MigrationManager{Db: db}
	migration, err := mm.GenerateMigration(
		"init",
		nil,
		ManyIndexes{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_migration),
		strings.TrimSpace(migration),
	)
}

func TestUniqContraintFailed(t *testing.T) {
	when_a := time.UnixMilli(1257894000000).UTC()
	when_b := time.UnixMilli(1257898000000).UTC()

	db, err := ss2.NewDb(":memory:")
	assert.Nil(t, err)
	err = ss2.MigrationManager{Db: db}.CreateTables(db, IndexedTbl{})
	assert.Nil(t, err)

	err = ss2.Exec(
		db,
		"INSERT INTO indexedtbl (key, val) VALUES (?, ?)",
		"time_a",
		when_a,
	)
	assert.Nil(t, err)

	err = ss2.Exec(
		db,
		"INSERT INTO indexedtbl (key, val) VALUES (?, ?)",
		"time_a",
		when_b,
	)
	assert.ErrorIs(t, err, ss2.ErrUniqConstraint)

	slice, err := ss2.FetchSlice[IndexedTbl](
		db,
		"SELECT id, key, val FROM indexedtbl",
	)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(slice))
	assert.Equal(t, "time_a", slice[0].Key)
	assert.Equal(t, when_a, slice[0].Val)
}
