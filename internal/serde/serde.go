package serde

import (
	"reflect"
	"strings"

	"gitlab.com/asvedr/ss2/internal/names"
	"gitlab.com/asvedr/ss2/internal/proto"
	"gitlab.com/asvedr/ss2/internal/stdtp"
)

type serde_str struct {
	tp     reflect.Type
	fields []serde_fld
}

type serde_fld struct {
	index int
	tp    reflect.Type
	serde proto.IAtomicSerDe
}

func NewTp(tp reflect.Type) proto.ISerDe {
	if tp.Kind() != reflect.Struct {
		panic("serde.New expect struct but found: " + tp.String())
	}
	return new_serde_str(tp)
}

func new_serde_str(tp reflect.Type) serde_str {
	self := serde_str{tp: tp}
	for i := range tp.NumField() {
		f := tp.Field(i)
		if strings.HasPrefix(f.Name, names.FieldMeta) {
			continue
		}
		serde := stdtp.Map[f.Type]
		if serde == nil {
			panic(f.Type.String() + " has no std conv")
		}
		mfield := serde_fld{
			index: i,
			tp:    f.Type,
			serde: serde,
		}
		self.fields = append(self.fields, mfield)
	}
	return self
}

func (self serde_str) ToParams(as_any any) []any {
	val := reflect.ValueOf(as_any)
	res := make([]any, len(self.fields))
	for i, field := range self.fields {
		fld_val := val.Field(field.index).Interface()
		res[i] = field.serde.ToParam(fld_val)
	}
	return res
}

func (self serde_str) Receivers() []any {
	res := make([]any, len(self.fields))
	for i, field := range self.fields {
		res[i] = field.serde.Receiver()
	}
	return res
}

func (self serde_str) FromParams(params []any) any {
	res := reflect.New(self.tp).Elem()
	for i, field := range self.fields {
		fld_val := field.serde.FromParam(params[i])
		if fld_val == nil {
			continue
		}
		reflected := reflect.ValueOf(fld_val)
		res.Field(field.index).Set(reflected)
	}
	return res.Interface()
}
