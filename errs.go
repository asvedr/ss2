package ss2

import (
	"gitlab.com/asvedr/ss2/internal/errs"
	"modernc.org/sqlite"
	sqlite3 "modernc.org/sqlite/lib"
)

var ErrNotFound = errs.ErrNotFound{}
var ErrUniqConstraint = errs.ErrUniqConstraint{}
var ErrForeignKey = errs.ErrForeignKey{}
var ErrInconsistentMigration = errs.ErrInconsistentMigration{}
var ErrInconsistentMigrationState = errs.ErrInconsistentMigrationState{}
var ErrMigrationNameDuplicated = errs.ErrMigrationNameDuplicated{}

func wrap_err(err error) error {
	sqliterr, casted := err.(*sqlite.Error)
	if !casted {
		return err
	}
	switch sqliterr.Code() {
	case sqlite3.SQLITE_CONSTRAINT_UNIQUE:
		return ErrUniqConstraint
	case sqlite3.SQLITE_CONSTRAINT_FOREIGNKEY:
		return ErrForeignKey
	default:
		return err
	}
}
