package ss2_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/asvedr/ss2"
)

type T1v1 struct {
	Id      int `pk:"t"`
	FOptStr *string
	Meta    struct {
		Name ss2.None `value:"t1"`
	}
}

type T2v1 struct {
	Id   int `pk:"t"`
	FInt int
	Meta struct {
		Name ss2.None `value:"t2"`
	}
}

type T2v2 struct {
	Id   int `pk:"t"`
	FInt int `index:"t"`
	Meta struct {
		Name ss2.None `value:"t2"`
	}
}

type T1v2 struct {
	Id      int     `pk:"t"`
	FReal   float64 `db_default:"1.5"`
	FOptStr *string
	Meta    struct {
		Name ss2.None `value:"t1"`
	}
}

type T1v3 struct {
	Id      int `pk:"t"`
	FReal   int
	FOptStr *string
	Meta    struct {
		Name ss2.None `value:"t1"`
	}
}

const expected_mig_v1 = `
name: v1
operations:
    - type: add_table
      name: t1
      new_columns:
        - name: id
          type: integer
          pk: true
          not_null: true
        - name: foptstr
          type: text
    - type: add_table
      name: t2
      new_columns:
        - name: id
          type: integer
          pk: true
          not_null: true
        - name: fint
          type: integer
          not_null: true
`

const expected_mig_v2 = `
name: v2
operations:
    - type: add_column
      table: t1
      new_column:
        name: freal
        type: real
        not_null: true
        db_default: "1.5"
`
const expected_drop = `
name: drop_t2
operations:
    - type: drop_table
      table: t2
`
const expected_redefine = `
name: redefine
operations:
    - type: redefine_column
      table: t1
      new_column:
        name: freal
        type: integer
        not_null: true
`

func TestMigrationEmpty(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	assert.NoError(t, mm.ApplyMigrations(nil))
}

func TestMigrationBadYaml(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	err := mm.ApplyMigrations([]string{"xxx"})
	assert.Error(t, err)
}

func TestMigrationV1(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration("v1", nil, T1v1{}, T2v1{})
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_mig_v1),
		strings.TrimSpace(mig),
	)
}

func TestNewNameDuplicated(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	_, err := mm.GenerateMigration("v1", []string{expected_mig_v1}, T1v1{})
	assert.ErrorAs(t, err, &ss2.ErrMigrationNameDuplicated)
}

func TestMigrationV2(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	m_v1, err := mm.GenerateMigration("v1", nil, T1v1{}, T2v1{})
	assert.Nil(t, err)
	mig, err := mm.GenerateMigration(
		"v2",
		[]string{m_v1},
		T2v1{},
		T1v2{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_mig_v2),
		strings.TrimSpace(mig),
	)
}

func TestDefaultColumnValue(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	err := mm.ApplyMigrations([]string{expected_mig_v1, expected_mig_v2})
	assert.NoError(t, err)
	ss2.Exec(db, "INSERT INTO t1 (id) VALUES (1)")
	row, err := ss2.FetchOne[T1v2](
		db,
		"SELECT id, freal, foptstr FROM t1",
	)
	assert.NoError(t, err)
	assert.Nil(t, row.FOptStr)
	assert.InDelta(t, 1.5, row.FReal, 0.01)
}

func TestDuplicateMigrationName(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	_, err := mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1, expected_mig_v1},
		T1v1{},
	)
	assert.ErrorAs(t, err, &ss2.ErrMigrationNameDuplicated)
}

func TestMigrationDropTable(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration(
		"drop_t2",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v2{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_drop),
		strings.TrimSpace(mig),
	)

	err = mm.ApplyMigrations([]string{
		expected_mig_v1, expected_mig_v2, mig,
	})
	assert.NoError(t, err)

	mig, err = mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1, expected_mig_v2, expected_drop},
		T1v2{},
	)
	assert.NoError(t, err)
	assert.Equal(t, "", mig)
}

func TestMigrationNoChanges(t *testing.T) {
	db, err := ss2.NewDb(":memory:")
	assert.Nil(t, err)
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v2{},
		T2v1{},
	)
	assert.Nil(t, err)
	assert.Equal(t, "", mig)
}

func TestMigrationRedefineColumn(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")

	mm := ss2.MigrationManager{Db: db}
	_, err := mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v3{},
		T2v1{},
	)
	assert.Equal(
		t,
		"Detected column change(table=t1):, freal real DEFAULT 1.5 not null -> freal integer not null",
		err.Error(),
	)

	mm = ss2.MigrationManager{Db: db, AllowRedefine: true}
	mig, err := mm.GenerateMigration(
		"redefine",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v3{},
		T2v1{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_redefine),
		strings.TrimSpace(mig),
	)

	err = mm.ApplyMigrations(
		[]string{expected_mig_v1, expected_mig_v2, mig},
	)
	assert.NoError(t, err)

	mm = ss2.MigrationManager{Db: db, AllowRedefine: true}
	mig, err = mm.GenerateMigration(
		"xx",
		[]string{expected_mig_v1, expected_mig_v2, expected_redefine},
		T1v3{},
		T2v1{},
	)
	assert.NoError(t, err)
	assert.Equal(t, "", mig)
}

func TestApplyMigration(t *testing.T) {
	// type T2v1 struct
	//   Id   int `pk:"t"`
	//   FInt int
	//   Meta {Name `value:"t2"`}

	// type T1v2 struct
	//   Id      int `pk:"t"`
	//   FReal   float64
	//   FOptStr *string
	//   Meta    {Name `value:"t1"`}

	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	err := mm.ApplyMigrations([]string{expected_mig_v1, expected_mig_v2})
	assert.Nil(t, err)
	err = ss2.Exec(
		db,
		`INSERT INTO t1 (freal, foptstr)
		 VALUES (1.0, 'abc'), (2.0, null)`,
	)
	assert.Nil(t, err)
	err = ss2.Exec(
		db,
		`INSERT INTO t2 (fint) VALUES (15)`,
	)
	assert.Nil(t, err)

	rows, err := ss2.FetchSlice[T1v2](
		db,
		"SELECT id, freal, foptstr FROM t1 ORDER BY id",
	)
	assert.Nil(t, err)
	assert.Equal(t, 2, len(rows))

	assert.InDelta(t, 1.0, rows[0].FReal, 0.001)
	assert.Equal(t, "abc", *rows[0].FOptStr)

	assert.InDelta(t, 2.0, rows[1].FReal, 0.001)
	assert.Nil(t, rows[1].FOptStr)

	t2_rows, err := ss2.FetchSlice[T2v1](
		db, "SELECT id, fint FROM t2 ORDER BY id",
	)
	assert.Nil(t, err)
	assert.Equal(t, []T2v1{{Id: 1, FInt: 15}}, t2_rows)

	mm = ss2.MigrationManager{Db: db}
	err = mm.ApplyMigrations([]string{expected_mig_v1, expected_mig_v2})
	assert.Nil(t, err)

	t2_rows, err = ss2.FetchSlice[T2v1](
		db, "SELECT id, fint FROM t2 ORDER BY id",
	)
	assert.Nil(t, err)
	assert.Equal(t, []T2v1{{Id: 1, FInt: 15}}, t2_rows)
}

const sql_mig = `
name: sql_migration
operations:
    - type: sql
      sql: "insert into t1 (freal) values (1.0)"
`

func TestSqlMigration(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	err := mm.ApplyMigrations(
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			sql_mig,
		},
	)
	assert.Nil(t, err)

	diff, err := mm.GenerateMigration(
		"xxx",
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			sql_mig,
		},
		T1v2{},
		T2v1{},
	)
	assert.Nil(t, err)
	assert.Equal(t, "", diff)
}

const expected_mig_add_index = `
name: add_index
operations:
    - type: add_index
      table: t2
      name: ix_t2__autofield_fint_ix
      columns: [fint]
`

const expected_mig_drop_index = `
name: drop_index
operations:
    - type: drop_index
      table: t2
      index: ix_t2__autofield_fint_ix
`

func TestAddIndex(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration(
		"add_index",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v2{},
		T2v2{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_mig_add_index),
		strings.TrimSpace(mig),
	)

	err = mm.ApplyMigrations(
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_add_index,
		},
	)
	assert.Nil(t, err)
}

func TestDropIndex(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration(
		"drop_index",
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_add_index,
		},
		T1v2{},
		T2v1{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_mig_drop_index),
		strings.TrimSpace(mig),
	)

	err = mm.ApplyMigrations(
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_drop_index,
		},
	)
	assert.Nil(t, err)

	mig, err = mm.GenerateMigration(
		"xxx",
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_add_index,
			expected_mig_drop_index,
		},
		T1v2{},
		T2v1{},
	)
	assert.NoError(t, err)
	assert.Equal(t, "", mig)

	_, err = mm.GenerateMigration(
		"xxx",
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			// modified_mig_add_index,
			expected_mig_drop_index,
		},
		T1v2{},
		T2v1{},
	)
	assert.Equal(
		t,
		"can not find index t2:ix_t2__autofield_fint_ix",
		err.Error(),
	)
}

const expected_mig_drop_column = `
name: drop_column
operations:
    - type: drop_column
      table: t1
      column: freal
`

func TestDropColumn(t *testing.T) {
	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	mig, err := mm.GenerateMigration(
		"drop_column",
		[]string{expected_mig_v1, expected_mig_v2},
		T1v1{},
		T2v1{},
	)
	assert.Nil(t, err)
	assert.Equal(
		t,
		strings.TrimSpace(expected_mig_drop_column),
		strings.TrimSpace(mig),
	)

	err = mm.ApplyMigrations(
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_drop_column,
		},
	)
	assert.NoError(t, err)

	mig, err = mm.GenerateMigration(
		"xxx",
		[]string{
			expected_mig_v1,
			expected_mig_v2,
			expected_mig_drop_column,
		},
		T1v1{},
		T2v1{},
	)
	assert.NoError(t, err)
	assert.Equal(t, "", mig)

	_, err = mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1, expected_mig_drop_column},
		T1v1{},
		T2v1{},
	)
	assert.Equal(t, "can not find column t1:freal", err.Error())
}

func TestAddColumnNotNullNoDefault(t *testing.T) {
	type T1vX struct {
		Id      int `pk:"t"`
		FReal   float64
		FOptStr *string
		Meta    struct {
			Name ss2.None `value:"t1"`
		}
	}

	db, _ := ss2.NewDb(":memory:")
	mm := ss2.MigrationManager{Db: db}
	_, err := mm.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1},
		T1vX{},
		T2v1{},
	)
	exp := "table t1, column freal not null without default"
	assert.Equal(t, exp, err.Error())
}

func TestGetMigrationNoDb(t *testing.T) {
	type T1vX struct {
		Id      int `pk:"t"`
		FReal   float64
		FOptStr *string
		Meta    struct {
			Name ss2.None `value:"t1"`
		}
	}

	_, err := ss2.MigrationManager{}.GenerateMigration(
		"xxx",
		[]string{expected_mig_v1},
		T1vX{},
		T2v1{},
	)
	exp := "table t1, column freal not null without default"
	assert.Equal(t, exp, err.Error())
}
